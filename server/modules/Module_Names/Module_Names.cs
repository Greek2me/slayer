//----------------------------------------------------------------------
// Title:   Module_Names
// Author:  Greek2me
// Version: 1
// Updated: January 21, 2023
//----------------------------------------------------------------------
// Loads a list of common names from a file and allows selection of a
// random name. The name list is derived from
// https://github.com/smashew/NameDatabases
//----------------------------------------------------------------------
// Include this code in your own scripts as an *individual file*
// called "Module_Names.cs". Do not modify this code.
//----------------------------------------------------------------------

// if($Module_Names::Version >= 1)
// 	return;
$Module_Names::Version = 1;

$Module_Names::Loaded = 1;
$Module_Names::NameFile = $Slayer::Server::Path @ "/modules/Module_Names/first-names.txt";

function getRandomFirstName()
{
	%idx = getRandom(0, $Modules_Names::NumNames);
	return $Modules_Names::Name[%idx];
}

function loadNamesFromFile(%path)
{
	%file = new FileObject();
	%file.openForRead(%path);
	
	%idx = 0;
	while(!%file.isEOF())
	{
		$Modules_Names::Name[%idx] = %file.readLine();
		%idx = %idx + 1;
	}
	$Modules_Names::NumNames = %idx;
	
	%file.close();
	%file.delete();
}

loadNamesFromFile($Module_Names::NameFile);