// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

package Slayer_Tutorial
{
	function Slayer_MiniGameSO::pickSpawnPoint(%this, %client)
	{
		if(%client.isHost && !$Slayer::Server::TutorialRun && isObject(_slayerInitialSpawn))
		{
			$Slayer::Server::TutorialRun = true;
			schedule(0, 0, deactivatePackage, Slayer_Tutorial);
			return _slayerInitialSpawn.getTransform();
		}
		return parent::pickSpawnPoint(%this, %client);
	}
};

if($Slayer::Server::CurGameModeArg $= $Slayer::Server::GameModeArg)
{
	activatePackage(Slayer_Tutorial);
}