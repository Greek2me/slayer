// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_PrefSO::onAdd(%this)
{
	%p = Slayer.Prefs.getPrefSO(%this.category, %this.title);
	if(isObject(%p))
	{
		%p.delete();
		Slayer_Support::Debug(1, "Overwriting preference", %this.category TAB %this.title);
	}
	else
	{
		Slayer_Support::Debug(2, "Slayer_PrefSO::onAdd", %this.category TAB %this.title);
	}
	
	
	//Minigame variables
	if(striPos(%this.variable, "%mini.") == 0)
	{
		%this.isMinigameVar = true;
		%this.variable = striReplace(%this.variable, "%mini.", "");
	}
	else if(%this.getValue() $= "")
	{
		%this.setValue(%this.defaultValue);
	}

	%this.schedule(0, onAdded, %this);
}

function Slayer_PrefSO::onAdded(%this)
{
	Slayer.Prefs.add(%this);
	if(isObject(missionCleanup) && missionCleanup.isMember(%this))
		missionCleanup.remove(%this);
}

//Sets preference to a specified value.
//@param	string value
//@param	Slayer_MinigameSO mini
//@return	bool	The value was set successfully.
function Slayer_PrefSO::setValue(%this, %value, %mini)
{
	%handler = %this.getGroup();
	%var = %this.variable;

	if(isObject(%mini))
	{
		if(!isSlayerMinigame(%mini))
			return false;

		if(%this.preload || %this.permissionLevel == $Slayer::PermissionLevel["Host"])
		{
			if(%mini != Slayer.Minigames.getHostMinigame())
				return false;
		}

		if(%this.isMinigameVar)
			%var = %mini @ "." @ %var;
	}
	else if(%this.isMinigameVar)
	{
		return false;
	}

	if(Slayer.isStarting || (isObject(%mini) && %mini.isStarting && %mini.isDedicatedStart))
	{
		if(%this.isMinigameVar)
		{
			if(Slayer.isStarting)
			{
				return false;
			}
		}
		else
		{
			if(%handler.setNonMinigamePrefs)
			{
				return false;
			}
		}
	}

	%proof = %this.idiotProof(%value);
	if(getField(%proof, 0))
		%value = getField(%proof, 1);
	else
		return false;

	%callback = %this.callback;
	if(%callback !$= "")
	{
		%callback = strReplace(%callback, "%1", "\"" @ %value @ "\"");
		%callback = strReplace(%callback, "%2", "\"" @ %this.getValue(%mini) @ "\"");

		if(strPos(%callback, "%mini.") >= 0 && !isObject(%mini))
			%callback = "";
		else
			%callback = strReplace(%callback, "%mini", %mini);
	}

	Slayer_Support::setDynamicVariable(%var, %value);

	Slayer_Support::Debug(2, "Preference Set", %this.category TAB %this.title TAB %value);

	if(%callback !$= "")
		eval(%callback);

	return true;
}

//@param	Slayer_MinigameSO mini
//@return	string	The value of this preference.
function Slayer_PrefSO::getValue(%this, %mini)
{
	%var = %this.variable;
	if(%this.isMinigameVar)
	{
		if(isSlayerMinigame(%mini))
			%var = %mini @ "." @ %var;
		else
		{
			if(%this.type $= "object" && isObject(%this.defaultValue))
				return nameToID(%this.defaultValue);
			else
				return %this.defaultValue;
		}
	}

	return Slayer_Support::getDynamicVariable(%var);
}