// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer::onAdd(%this)
{
	%this.isStarting = true;

	%this.add(%this.gameModes	= new ScriptGroup(Slayer_GamemodeHandlerSG));
	%this.add(%this.miniGames	= new ScriptGroup(Slayer_MiniGameHandlerSG));
	%this.add(%this.pathHandler	= new ScriptGroup(Slayer_PathHandlerSG));
	%this.add(%this.prefs		= new ScriptGroup(Slayer_PrefHandlerSG));
	%this.add(%this.teamPrefs	= new ScriptGroup(Slayer_TeamPrefHandlerSG));
	
	%this.add(%this.spawns			= new SimSet());
	%this.add(%this.vehicleSpawns	= new SimSet());
	%this.add(%this.slayerBricks	= new SimSet());
	%this.add(%this.capturePoints	= new SimSet());
	%this.add(%this.boundaryBricks	= new SimSet());
	
	//CLIENT AUTHENTICATION - Just in case people connected before Slayer loaded
	for(%i = 0; %i < clientGroup.getCount(); %i ++)
		%this.initClientAuth(clientGroup.getObject(%i));

	%this.isStarting = false;
}

function Slayer::onRemove(%this)
{
	//minigames need to be deleted first
	%this.Minigames.delete();

	Slayer_Support::Debug(0, "Deactivating Slayer");

	//close the debug logger
	if(isObject(%this.debugFO))
		%this.debugFO.close();

	deleteVariables("$Slayer::Server::*");
	$Slayer::Server = "";
}

function Slayer::initClientAuth(%this, %client)
{
	%client.isHost = (%client.isLocalConnection() || %client.getBLID() == Slayer_Support::getBlocklandID());

	//tell the client that Slayer is enabled
	Slayer_Support::Debug(0, "Sending Handshake", %client.getSimpleName());
	commandToClient(%client, 'Slayer_Handshake', $Slayer::Server::Version, $Slayer::Server::ModVersions);
}

function Slayer::onClientHandshakeAccepted(%this, %client, %version, %modVersions)
{
	//Slayer_Support::Debug(0, "Handshake ACCEPTED", %client.getSimpleName());

	//TELL THE CLIENT
	commandToClient(%client, 'Slayer_getInitialData_Start');
}

function Slayer::onClientHandshakeDenied(%this, %client, %version, %modVersions, %reason)
{
	%client.slayer = false;
	switch$(%reason)
	{
		case "":
			%reason = "reason unspecified";

		case "version too old":
			commandToClient(%client, 'MessageBoxOK', "Slayer",
				"Your copy of Slayer is out-of-date and will not work properly." @
				"\n\nPlease get the latest version:" @
				"\n<a:" @ $Slayer::URL::Download @ ">Download Now</a>");
	}
	Slayer_Support::Debug(0, "Handshake DENIED (" @ %reason @ ")", %client.getSimpleName());
}

function Slayer::sendInitialData(%this, %client)
{
	Slayer_Support::Debug(0, "Sending Initial Data", %client.getSimpleName());

	//START THE INITIAL DATA TRANSFER
	commandToClient(%client, 'Slayer_getInitialData_Start');

	//SEND GAME MODES
	%this.Gamemodes.sendGameModes(%client);
	
	//SEND PRERERENCES
	%this.Prefs.sendPreferenceData(%client);
	%this.TeamPrefs.sendPreferenceData(%client);

	//END THE INITIAL DATA TRANSFER
	commandToClient(%client, 'Slayer_getInitialData_End');

	%client.slayerInitialDataSent = true;

	%this.onInitialDataSent(%client);
}

function Slayer::onInitialDataSent(%this, %client)
{

}

function Slayer::sendData(%this, %client, %mini, %openGUI)
{
	if(!isObject(%mini) && %mini != -1)
		%mini = getMinigameFromObject(%client);

	if(isSlayerMinigame(%mini) && !%mini.canEdit(%client))
		return;

	//SEND INITIAL DATA
	if(!%client.slayerInitialDataSent)
		%this.sendInitialData(%client);

	Slayer_Support::Debug(1, "Sending Data", %client.getSimpleName());

	%client.editingMinigame = %mini;

	if(isSlayerMinigame(%mini))
		%minigameState = $Slayer::MinigameState["Edit"];
	else
		%minigameState = $Slayer::MinigameState["Create"];

	//START THE DATA TRANSFER
	commandToClient(%client, 'Slayer_getData_Start', %mini, %client.getAdminLvl(), %minigameState);

	//SEND PRERERENCE VALUES
	%this.Prefs.sendPreferenceValues(%client, %mini);
	%this.TeamPrefs.sendPreferenceValues(%client, %mini);

	//SEND TEAMS
	if(isObject(%mini.Teams))
		%mini.Teams.sendTeams(%client);

	//END THE DATA TRANSFER
	commandToClient(%client, 'Slayer_getData_End', %openGUI);

	%this.onDataSent(%client, %mini, %openGUI);
}

function Slayer::onDataSent(%this, %client, %mini, %openGUI)
{

}

function serverCmdSlayer_Handshake(%client, %version, %modVersions)
{
	if(%client.isSpamming())
	{
		if(!%client.slayer_handshakeSpam)
		{
			Slayer_Support::Error("serverCmdSlayer_Handshake", %client.getSimpleName() SPC "is shaking hands like crazy!");
			%client.slayer_handshakeSpam = true;
		}
		return;
	}

	%client.slayerVersion = %version;

	%compare = semanticVersionCompare($Slayer::Server::Version, %version);
	if(%compare == 1)
	{
		messageClient(%client, '', "\c0WARNING: \c6You have an \c3older \c6version of \c3Slayer \c6than the server." SPC 
			"You can download the latest version <a:" @ $Slayer::URL::Download @ ">here</a>\c6.");
		%compat = semanticVersionCompare($Slayer::Server::CompatibleVersion, %version);
		if(%compat == 1 || !strLen($Slayer::Server::CompatibleVersion))
		{
			Slayer.onClientHandshakeDenied(%client, %version, %modVersions, "version too old");
			return;
		}
		else
			messageClient(%client, '', "\c0 + \c6You are still able to use the Slayer GUI, but some features may not work correctly.");
	}
	else if(%compare == 2)
	{
		Slayer.onClientHandshakeDenied(%client, %version, %modVersions, "version too new");
		return;
	}

	%client.slayer = 1;

	Slayer_Support::Debug(0, "Slayer client registered", %client.getSimpleName() SPC "has version" SPC %version);

	if($Slayer::Server::ModVersions !$= "" && %modVersions !$= "")
	{
		for(%i=0; %i < getFieldCount(%modVersions); %i++)
		{
			%field = getField(%modVersions, %i);
			%mod = firstWord(%field);
			%mv = restWords(%field);

			for(%e=0; %e < getFieldCount($Slayer::Server::ModVersions); %e++)
			{
				%field = getField($Slayer::Server::ModVersions, %e);
				if(%mod $= firstWord(%field))
				{
					%client.slayerModVersion_[%mod] = %mv;
					if(%mv $= restWords(%field))
					{
						%client.slayerMod_[%mod] = 1;
					}

					Slayer_Support::Debug(0, " + Client mod registered", %mod SPC "version" SPC %mv);
				}
			}
		}
	}

	Slayer.onClientHandshakeAccepted(%client, %version, %modVersions);
}

function serverCmdSlayer_sendMinigameState(%client)
{
	if(!%client.slayer || %client.isSpamming())
		return;
	commandToClient(%client, 'Slayer_getMinigameState', %client.getMinigameState());
	Slayer.Minigames.broadcastAllMinigames(%client, 1);
}

package Slayer
{
	function destroyServer()
	{
		if(isObject(Slayer))
			Slayer.delete();

		$Slayer::Platform = "Client";

		parent::destroyServer();
	}

	function onExit()
	{
		if(isObject(Slayer))
			Slayer.delete();

		parent::onExit();
	}
};
activatePackage(Slayer);