// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

if($Pref::Slayer::Server::CreateBrickUISlayerTab)
{
	$Slayer::Server::Bricks::Category = "Slayer";
	$Slayer::Server::Bricks::SubCategory = "";
}
else
{
	$Slayer::Server::Bricks::Category = "Special";
	$Slayer::Server::Bricks::SubCategory = "Slayer - ";
}


if($Slayer::Server::DatablocksLoaded)
	return;

$Slayer::Server::DatablocksLoaded = true;

datablock TriggerData(Slayer_CPTriggerData)
{
	tickPeriodMS = $Pref::Slayer::Server::CPTriggerTickMS;
};

datablock PathCameraData(Slayer_SpectatePathCamData : Observer)
{
	maxNodes = 20;
	defaultPath = "Spline";
	defaultType = "Normal";
	defaultSpeed = 7;
};

datablock FxDtsBrickData(brickSlyrSpawnPointData : brickSpawnPointData)
{
	uiName = "Team Spawn Point";
	category = $Slayer::Server::Bricks::Category;
	subCategory = $Slayer::Server::Bricks::SubCategory @ "General";

	isSlyrBrick = true;
	slyrType = "TeamSpawn";
	setOnceOnly = false;
};

datablock FxDtsBrickData(brickSlyrVehicleSpawnData : brickVehicleSpawnData)
{
	uiName = "Team Vehicle Spawn";
	category = $Slayer::Server::Bricks::Category;
	subCategory = $Slayer::Server::Bricks::SubCategory @ "General";

	isSlyrBrick = true;
	slyrType = "TeamVehicle";
	setOnceOnly = false;
};

datablock FxDtsBrickData(brickSlyrCPData : brick8x8FData)
{
	uiName = "8x8 Capture Point";
	category = $Slayer::Server::Bricks::Category;
	subCategory = $Slayer::Server::Bricks::SubCategory @ "General";
	indestructable = 1;

	isSlyrBrick = true;
	slyrType = "CP";
	setOnceOnly = true;
	CPMaxTicks = 40;
};

datablock FxDtsBrickData(brickSlyrLrgCPData : brick16x16FData)
{
	uiName = "16x16 Capture Point";
	category = $Slayer::Server::Bricks::Category;
	subCategory = $Slayer::Server::Bricks::SubCategory @ "General";
	indestructable = 1;

	isSlyrBrick = true;
	slyrType = "CP";
	setOnceOnly = true;
	CPMaxTicks = 66;
};

datablock FxDtsBrickData(brickSlyrRegionBoundaryData)
{
	uiName = "Region Boundary";
	category = $Slayer::Server::Bricks::Category;
	subCategory = $Slayer::Server::Bricks::SubCategory @ "General";
	brickFile = $Slayer::Server::Path @ "/core/resources/shapes/brickSlayer3x3x15.blb";
	indestructable = 1;
	isSlyrBrick = true;
	slyrType = "RegionBoundary";
};

datablock FxDtsBrickData(brickSlyrBotPathNodeData)
{
	uiName = "Path Node";
	category = $Slayer::Server::Bricks::Category;
	subCategory = $Slayer::Server::Bricks::SubCategory @ "Bot Holes";
	brickFile = $Slayer::Server::Path @ "/core/resources/shapes/brickSlayer3x3x15.blb";
	indestructable = 1;

	isSlyrBrick = true;
	slyrType = "BotPathNode";
	setOnceOnly = true;
};

datablock FxDtsBrickData(brickSlyrBotRallyNodeData : brickSlyrBotPathNodeData)
{
	isRallyNode = true;
	uiName = "Rally Point";
};

//@private
function Slayer_loadHoleBotDatablocks()
{
	%count = datablockGroup.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%db = datablockGroup.getObject(%i);
		if(%db.isBotHole && !%db.isSlyrBrick)
		{
			%dbName = %db.getName();
			%db.setName("Slayer_BotHole");
			datablock FxDtsBrickData(Slayer_TeamBotHole : Slayer_BotHole)
			{
				category = $Slayer::Server::Bricks::Category;
				subCategory = $Slayer::Server::Bricks::SubCategory @ "Bot Holes";
				uiName = "Team" SPC %db.uiName;

				isSlyrBrick = true;
				slyrType = "TeamBotHole";
				setOnceOnly = false;
			};
			Slayer_TeamBotHole.setName("Slayer_Team" @ %dbName);
			%db.setName(%dbName);
		}
	}

	$Slayer::Server::Bricks::HoleBotDatablocksLoaded = 1;
}
//if($Pref::Slayer::Server::CreateTeamBotHoles)
//	scheduleNoQuota(0, 0, "Slayer_loadHoleBotDatablocks");