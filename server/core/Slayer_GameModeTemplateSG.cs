// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_GameModeTemplateSG::onAdd(%this)
{
	Slayer_Support::Debug(2, "Slayer_GameModeTemplateSG::onAdd", %this.uiName);
	if(%this.className $= "")
	{
		Slayer_Support::error("Invalid game mode class - please use a unique class name.");
		%this.schedule(0, delete);
		return;
	}
	%this.schedule(0, onAdded, %this);
}

function Slayer_GameModeTemplateSG::onAdded(%this)
{
	Slayer.gameModes.add(%this);
	Slayer.gameModes.template[%this.className] = %this;
	if(isObject(missionCleanup) && missionCleanup.isMember(%this))
		missionCleanup.remove(%this);
}