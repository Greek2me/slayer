// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//Sends the bot to an objective.
//@param	FxDtsBrick|Grid3D objective
//@return	FxDtsBrick	The path the bot will use.
//@see	AiController::goToNextObjective
//@see	Slayer_onBotPathFound
function AiPlayer::goToObjective(%this, %objective)
{
	%position = vectorAdd(%this.position, "0 0 0.1");
	%closestNode = Slayer.PathHandler.findClosestNode(%position);
	if(!isObject(%closestNode))
	{
		Slayer_Support::Error("AiPlayer::goToObjective", "No nearby node found!");
		return 0;
	}

	%this.objective = %objective;
	%this.objectiveBrick = %this.client.objectiveBrick[%this.client.objectiveID[%objective]];
	%this.objectiveReached = false;

	if(%objective.position $= "")
	{
		Slayer_Support::Error("AiPlayer::goToObjective", "Invalid objective, position undefined");
		return 0;
	}
	else if(!Slayer.PathHandler.nodes.isMember(%objective))
	{
		Slayer.PathHandler.addNode(%objective);
	}

	%this.hPathBrick = 0;

	%path = Slayer.PathHandler.findPath(%closestNode, %objective, "Slayer_onBotPathFound");
	%path.bot = %this;
	%this.path = %path;

	Slayer_Support::Debug(2, "AiPlayer::goToObjective", "Using path" SPC %path);

	return %path;
}

//Sends the bot to the next node in their path.
//@return	fxDtsBrick	The next node in the path.
//@private
function AiPlayer::pathStep(%this)
{
	if(!isObject(%this.path))
		return 0;

	%node = getWord(%this.path.result, %this.pathIndex);
	if(!isObject(%node))
		return 0;

	Slayer_Support::Debug(2, "AiPlayer::pathStep", %this.pathIndex SPC %node);

	%this.pathNodeNext = %node;
	%this.hPathBrick = %node;

	cancel(%this.hSched);
	%this.hSched = 0;

	%this.hLoop();
	%this.hSkipOnReachLoop = true;

	return %node;
}

//Causes the AiPlayer to equip a pseudo-random tool from their inventory.
//Lower items in the tool array will have a higher probability of being chosen.
//For example, tool[0] has a higher probability than tool[4].
//@return	Item	The tool that was selected.
function AiPlayer::useRandomTool(%this)
{
	%maxTools = %this.getDatablock().maxTools;
	for(%i = 0; %i < %maxTools; %i ++)
	{
		if(isObject(%this.tool[%i]))
			%indexEnd = %i + 1;
	}

	for(%i = 0; %i < %indexEnd; %i ++)
	{
		if(isObject(%this.tool[%i]) && (getRandom(0, 1) || %i == %indexEnd - 1))
		{
			%this.setWeapon(%this.tool[%i]);
			return %this.tool[%i];
		}
	}

	return 0;
}

//Allowing for our hack...
//@see	AiPlayer::setTeam
function AiPlayer::getTeam(%this)
{
	if(isObject(%this.slyrTeam))
		return %this.slyrTeam;
	else
		return 0;
}

//Detects when a bot has reached the node.
function Grid3DNode::onBotReachBrick(%this, %bot)
{
	%ai = %bot.client;

	if(%bot.objective == %this)
	{
		if(!%bot.objectiveReached)
		{
			%bot.objectiveReached = true;
			%ai.onObjectiveReached(%this);
		}
	}
	// else if(%mini.bots_objectivesEnRoute && %ai.isObjective(%node))
	// {
		// if(!%bot.objectiveReached)
		// {
			// %bot.objective = %node;
			// %bot.objectiveReached = true;
			// %ai.onObjectiveReached(%node);
		// }
	// }
	else
	{
		%bot.pathIndex ++;
		%bot.pathStep();
	}
}

//holebots hack, please ignore
function Grid3DNode::hGetPosZ(%this)
{
	return FxDtsBrick::hGetPosZ(%this);
}

//Called when a path has been completely calculated.
//@param	ScriptObject path	The path object.
//@param	string result	A space-delimited list of nodes in the path.
//@see	AiPlayer::goToObjective
//@private
function Slayer_onBotPathFound(%path, %result)
{
	%bot = %path.bot;
	if(!isObject(%bot) || %bot.path != %path)
		return;

	%ai = %bot.client;
	if(!isObject(%ai))
		return;

	if(!strLen(%result) || %result $= "error")
	{
		%ai.onObjectiveFailed(%bot.objective);
		%path.delete();
	}
	else
	{
		//Check if the bot is already at the first node
		%start = firstWord(%result);
		%trigger = %start.nodeTrigger;
		if(isObject(%trigger))
		{
			for(%i = 0; %i < %trigger.getNumObjects(); %i ++)
			{
				%obj = %trigger.getObject(%i);
				if(%obj == %bot)
				{
					%skip0 = true;
					break;
				}
			}
		}

		//Disable wandering while on the path.
		%bot.hOldWander = %bot.hWander;
		%bot.hWander = false;

		%bot.pathIndex = (%skip0 ? 1 : 0);
		%bot.pathStep();
	}
}

package Slayer_AiPlayer
{
	function AiPlayer::hLoop(%this)
	{
		%mini = getMinigameFromObject(%this);
		if(isSlayerMinigame(%mini))
		{
			//Prevent the bot from being deleted when its owner leaves.
			%itemPos = %this.spawnBrick.itemPosition;
			%this.spawnBrick.itemPosition = 1;

			//Allow bots to work when "Include All Players' Bricks" is disabled.
			%miniOwner = %mini.owner;
			%mini.owner = %this.spawnBrick.getGroup().client;
			
			//Path following
			if(%this.isSlayerBot)
			{
				//Stop stopping!
				if(%this.hPathBrick && %this.hPathBrick == %this.pathNodeNext)
					%this.setMoveSlowDown(0);
			}

			%parent = parent::hLoop(%this);

			if(%this.isSlayerBot)
			{
				//Course correction
				if(%this.hOffCourse && (%this.hState !$= "Following" || !isObject(%this.hFollowing)))
				{
					%this.hOffCourse = false;
					%this.goToObjective(%this.objective);
				}

				//Check if we are off-course
				if((%this.hState $= "Following" || isObject(%this.hFollowing)) && isObject(%this.objective) && !%this.objectiveReached)
					%this.hOffCourse = true;
			}

			//Restore the old values.
			%this.spawnBrick.itemPosition = %itemPos;
			%mini.owner = %miniOwner;

			return %parent;
		}
		else
		{
			return parent::hLoop(%this);
		}
	}

	//Used by events to change the bot's name.
	//@param	string name	The new name of the bot.
	function AiPlayer::setBotName(%this, %name)
	{
		parent::setBotName(%this, %name);
		if(%this.isHoleBot)
			%this.hName = %name;
		if(%this.isSlayerBot && isObject(%this.client))
			%this.client.hName = %name;
	}

	//This is a terrible hack to set uniforms.
	//@private
	function AiPlayer::setTeam(%this, %mode, %teamName)
	{
		%parent = parent::setTeam(%this, %mode, %teamName);
		if(%mode == 5 && isSlayerMinigame(%mini = getMinigameFromObject(%this)))
		{
			%team = %mini.teams.getTeamFromName(%teamName);
			if(isObject(%team))
			{
				%this.slyrTeam = %team;
				if(%this.getDatablock().getID() == nameToID(BlockheadHoleBot))
				{
					GameConnection::applyUniform(%this); //hack!
				}
			}
		}
		return %parent;
	}

	//Determines whether hole bots are on the same team.
	//This is for hole bot compatibility.
	//@see minigameCanDamage
	//@private
	function checkHoleBotTeams(%obj, %target, %neutralAttack, %melee)
	{
		%mini = getMinigameFromObject(%obj);
		if(isSlayerMinigame(%mini) && %mini.gameMode.template.useTeams)
		{
			%clientA = Slayer_Support::getClientFromObject(%obj);
			%clientB = Slayer_Support::getClientFromObject(%target);
			%teamA = (isObject(%clientA) ? %clientA.getTeam() : 0);
			%teamB = (isObject(%clientB) ? %clientB.getTeam() : 0);
			if(isObject(%teamA))
			{
				if(%teamA.isAlliedTeam(%teamB))
					return 0;
			}
		}
		return parent::checkHoleBotTeams(%obj, %target, %neutralAttack, %melee);
	}

//	function fxDtsBrick::onBotSpawn(%this)
//	{
//		parent::onBotSpawn(%this);
//
//		%bot = %this.hBot;
//		if(isObject(%bot))
//		{
//			%mini = getMinigameFromObject(%this);
//			if(isSlayerMinigame(%mini))
//			{
//				if(!isObject(%bot.client))
//				{
//					if(!isObject(%this.aiClient))
//					{
//						%this.aiClient = new AiController()
//						{
//							bl_id = getBL_IDFromObject(%this);
//							spawnBrick = %this;
//							isHoleBot = 1;
//						};
//					}
//					%bot.client = %this.aiClient;
//					%bot.client.player = %bot;
//					%bot.client.bot = %bot;
//				}
//
//				if(!%mini.isMember(%bot.client))
//					%mini.addMember(%bot.client);
//
//				if(%this.getDatablock().slyrType $= "TeamBotHole")
//				{
//					%color = %this.getColorID();
//					%team = getField(%mini.Teams.getTeamsFromColor(%color), 0);
//					if(isObject(%team) && %team != %bot.client.slyrTeam)
//						%team.addMember(%bot.client, "", 1);
//					if(isObject(%bot.client.slyrTeam))
//					{
//						%bot.client.applyUniform();
//						if(%mini.bots_autoSetType)
//							%bot.hType = "mercenary";
//					}
//				}
//			}
//		}
//	}

	//Prevent a bunch of console spam.
	function AiPlayer::onCameraEnterOrbit(%this, %camera)
	{
		
	}

	//Prevent a bunch of console spam.
	function AiPlayer::onCameraLeaveOrbit(%this, %camera)
	{
		
	}

	//@deprecated
	function fxDtsBrick::spawnHoleBot(%this)
	{
		if(!%this.getDatablock().isBotHole || (isObject(%this.aiClient) && %this.aiClient.dead()))
			return 0;
		else
			return parent::spawnHoleBot(%this);
	}
};
activatePackage(Slayer_AiPlayer);