// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function AiController::onAdd(%this)
{
	%this.validateAvatarPrefs();
}

function AiController::onRemove(%this)
{
	if(isObject(%this.minigame))
		%this.minigame.removeMember(%this);
	if(isObject(%this.camera))
		%this.camera.delete();
	if(isObject(%this.player))
		%this.player.delete();
	if(isObject(%this.tempBrick))
		%this.tempBrick.delete();
	if(isObject(%this.brickGroup) && %this.brickGroup.client == %this)
		%this.brickGroup.client = -1;
}

function AiController::getClassName(%this)
{
	return "AiController";
}

function AiController::getPlayerName(%this)
{
	return %this.name;
}

function AiController::getSimpleName(%this)
{
	return %this.name;
}

//Creates an AiPlayer object for the AiController.
//@param	transform transform	The initial transform for the new player.
//@return	AiPlayer	The AiPlayer object that was created.
//@see	AiController::spawnPlayer
//@private
function AiController::createPlayer(%this, %transform)
{
	%player = new AiPlayer()
	{
		client = %this;
		datablock = PlayerStandardArmor;
		position = getWords(%transform, 0, 2);
		rotation = getWords(%transform, 3, 6);
	};
	%this.player = %player;
	missionCleanup.add(%player);
	return %player;
}

//Picks a spawn point and then spawns an AiPlayer.
//@return	AiPlayer	The AiPlayer object that was created.
//@see	AiController::onSpawn
//@see	AiController::createPlayer
function AiController::spawnPlayer(%this)
{
	if(isObject(%this.player))
		%this.player.delete();
	%mini = getMinigameFromObject(%this);
	if(isObject(%mini))
		%spawnPoint = %mini.pickSpawnPoint(%this);
	else
		%spawnPoint = pickSpawnPoint();
	%player = %this.createPlayer(%spawnPoint);
	if(!isObject(%player))
		return 0;
	%this.onSpawn();
	return %player;
}

//Called after an AiPlayer has been spawned.
function AiController::onSpawn(%this)
{

}

//Called when an AiController's AiPlayer is killed.
function AiController::onDeath(%this, %obj, %killer, %type, %area)
{
	return GameConnection::onDeath(%this, %obj, %killer, %type, %area);
}

//Set the AI's body colors.
function AiController::applyBodyColors(%this)
{
	return GameConnection::applyBodyColors(%this);
}

//Set the AI's body parts.
function AiController::applyBodyParts(%this)
{
	return GameConnection::applyBodyParts(%this);
}

//Validates AI avatar prefs.
function AiController::validateAvatarPrefs(%this)
{
	return GameConnection::validateAvatarPrefs(%this);
}

//Get AI score.
function AiController::getScore(%this)
{
	return GameConnection::getScore(%this);
}

//Set AI score.
function AiController::setScore(%this, %score)
{
	return GameConnection::setScore(%this, %score);
}

//Increase AI score.
function AiController::incScore(%this, %score)
{
	return GameConnection::incScore(%this, %score);
}

function AiController::bottomPrint() {}
function AiController::centerPrint() {}
function AiController::chatMessage() {}
function AiController::clearEventObjects() {}
function AiController::resetVehicles() {}
function AiController::getControlObject() { }
