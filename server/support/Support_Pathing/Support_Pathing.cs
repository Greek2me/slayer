//Author: Port
//BLID: 12297

exec("./BaseFinder.cs");
exec("./finders/AStarFinder.cs");

function calculatePath(%a, %b, %callback, %heuristic, %finder) {
	if (!isObject(%a) || !isObject(%b)) {
		error("ERROR: Invalid targets.");
		return -1;
	}

	if (%a.neighbors !$= "" && %a.linkCount $= "") {
		warn("WARN: Please switch your node system to set linkCount and link[i] for neighbors.");
		return -1;
	}

	if (%heuristic $= "") {
		%heuristic = "euclideanPathCost";
	}

	if (!isFunction(%heuristic)) {
		error("ERROR: Invalid heuristic.");
		return -1;
	}

	if (%finder $= "") {
		%finder = AStarFinder;
	}

	if (!isFunction(%finder, "onAdd")) {
		error("ERROR: Invalid finder.");
		return -1;
	}

	%a = %a.getID();
	%b = %b.getID();

	%obj = new ScriptObject() {
		class = %finder;
		superClass = BaseFinder;

		heuristic = %heuristic;
		callback = %callback;

		a = %a;
		b = %b;
	};

	return %obj;
}

// Built-in path heuristics.

function euclideanPathCost(%a, %b) {
	return vectorDist(%a, %b);
}

function manhattanPathCost(%a, %b) {
	%x = mAbs(getWord(%a, 0) - getWord(%b, 0));
	%y = mAbs(getWord(%a, 1) - getWord(%b, 1));
	%z = mAbs(getWord(%a, 2) - getWord(%b, 2));

	return %x + %y + %z;
}

function chebyshevPathCost(%a, %b) {
	%x = mAbs(getWord(%a, 0) - getWord(%b, 0));
	%y = mAbs(getWord(%a, 1) - getWord(%b, 1));
	%z = mAbs(getWord(%a, 2) - getWord(%b, 2));

	if (%x > %y && %x > %z) {
		return %z;
	}

	if (%y > %x && %y > %z) {
		return %y;
	}

	return %x;
}

new ScriptObject(PathingTempSO) {
	class = AStarFinder;
	superClass = BaseFinder;
};
PathingTempSO.delete();