//----------------------------------------------------------------------
// Title:   Support_MultiSourceEvents
// Author:  Greek2me
// Version: 1
// Updated: December 30, 2014
//----------------------------------------------------------------------
// Create and call events that may be located on many bricks. An example
// is the onMiniGameReset event.
//----------------------------------------------------------------------
// Include this code in your own scripts as an *individual file*
// called "Support_MultiSourceEvents.cs". Do not modify this code.
//----------------------------------------------------------------------

if($Support_MultiSourceEvents::Version > 1 && !$Debug)
	return;
$Support_MultiSourceEvents::Version = 1;

//Processes an event which occurs on multiple sources, normally bricks. 
//@param	string inputEvent	The name of the input event.
//@param	GameConnection client	The client that activated the event.
//@param	ScriptObject minigame	Optional. Only bricks within this minigame will be processed.
function processMultiSourceInputEvent(%inputEvent, %client, %minigame)
{
	%group = "multiSourceEventGroup" @ inputEvent_GetInputEventIdx(%inputEvent);

	if(!isObject(%group))
		return;

	for(%i = 0; %i < %group.getCount(); %i ++)
	{
		%obj = %group.getObject(%i);

		if(!%minigame || %minigame == getMinigameFromObject(%obj))
		{
			$InputTarget_["Self"] = %obj;
			%obj.processInputEvent(%inputEvent, %client);
		}
	}
}

//Registers a multiple-source input event. Use this in place of registerInputEvent.
//@param	string class	The object class that this event is far.
//@param	string inputEvent	The name of the input event.
//@param	string targets	A tab-delimited list of targets. See the link below.
//@link	http://forum.blockland.us/index.php?topic=40631.0
function registerMultiSourceInputEvent(%class, %inputEvent, %targets, %adminOnly)
{
	registerInputEvent(%class, %inputEvent, %targets, %adminOnly);

	//for whatever reason, this doesn't take a class parameter
	%inputEventIdx = inputEvent_GetInputEventIdx(%inputEvent);
	
	if(%inputEventIdx != -1)
		$InputEvent_MultiSource[%class, %inputEventIdx] = true;
}

package Support_MultiSourceEvents
{
	function serverCmdAddEvent(%client, %enabled, %inputEventIdx, %delay, %targetIdx, %namedTargetNameIdx, %outputEventIdx, %par1, %par2, %par3, %par4)
	{
		%obj = %client.wrenchBrick;
		%class = %obj.getClassName();

		parent::serverCmdAddEvent(%client, %enabled, %inputEventIdx, %delay, %targetIdx, %namedTargetNameIdx, %outputEventIdx, %par1, %par2, %par3, %par4);

		echo("Adding event.");
		if($InputEvent_MultiSource[%class, %inputEventIdx])
		{
			%group = "multiSourceEventGroup" @ %inputEventIdx;
			if(!isObject(%group))
			{
				new SimSet(%group);
				missionCleanup.add(%group);
			}
			echo(%group);
			%group.add(%obj);
		}
	}

	function SimObject::clearEvents(%this)
	{
		%class = %this.getClassName();
		for(%i = 0; %i < %this.numEvents; %i ++)
		{
			%inputEventIdx = %this.eventInputIdx[%i];
			if(!%removed[%inputEventIdx] && $InputEvent_MultiSource[%class, %inputEventIdx])
			{
				%removed[%inputEventIdx] = true;
				%group = "multiSourceEventGroup" @ %inputEventIdx;
				%group.remove(%this);
			}
		}
		return parent::clearEvents(%this);
	}
};
activatePackage(Support_MultiSourceEvents);