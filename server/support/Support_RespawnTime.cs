//----------------------------------------------------------------------
// Title:   Support_RespawnTime
// Author:  Greek2me
// Version: 1
// Updated: 
//----------------------------------------------------------------------
// Control player respawn times on an individual basis.
//----------------------------------------------------------------------
// Include this code in your own scripts as an *individual file*
// called "Support_RespawnTime.cs". Do not modify this code.
//----------------------------------------------------------------------

if($Support_RespawnTime::Version > 1 && !$Debug)
	return;
$Support_RespawnTime::Version = 1;

//Sets the amount of time that a client must wait 
// after dying before they can respawn.
//@param	int time	time in milliseconds
//@see	GameConnection::incRespawnTime
function GameConnection::setRespawnTime(%this, %time)
{
	%this.respawnTime = %time;
}

//Increases the amount of time that a client must wait 
// after dying before they can respawn.
//@param	int time	time in milliseconds
//@see	GameConnection::setRespawnTime
function GameConnection::incRespawnTime(%this, %time)
{
	if(%this.respawnTime $= "")
	{
		%mini = getMinigameFromObject(%this);
		if(isObject(%mini))
			%this.respawnTime = %mini.respawnTime;
		else
			%this.respawnTime = $Game::MinRespawnTime;
	}

	%this.setRespawnTime(%this.respawnTime + %time);
}

//Restores the client's respawn time to its original 
// minigame/default value.
function GameConnection::resetRespawnTime(%this)
{
	%this.respawnTime = "";
}

package Support_RespawnTime
{
	function GameConnection::onDeath(%this, %obj, %killer, %type, %area)
	{
		%this.lastDeathTime = getSimTime();

		if(%this.respawnTime $= "")
		{
			parent::onDeath(%this, %obj, %killer, %type, %area);
		}
		else
		{
			%mini = getMinigameFromObject(%this);
			if(%isMini = isObject(%mini))
			{
				%oldTime = %mini.respawnTime;
				%mini.respawnTime = %this.respawnTime;
			}
			else
			{
				%oldTime = $Game::MinRespawnTime;
				$Game::MinRespawnTime = %this.respawnTime;
			}

			parent::onDeath(%this, %obj, %killer, %type, %area);

			if(%isMini)
				%mini.respawnTime = %oldTime;
			else
				$Game::MinRespawnTime = %oldTime;
		}
	}

	function Observer::onTrigger(%this, %camera, %button, %state)
	{
		if(!%state || !isObject(%camera))
			return parent::onTrigger(%this, %camera, %button, %state);

		%client = %camera.getControllingClient();

		if(!isObject(%client) || %client.respawnTime $= "")
			return parent::onTrigger(%this, %camera, %button, %state);

		if(isObject(%client.player))
			return parent::onTrigger(%this, %camera, %button, %state);

		if(getSimTime() - %client.lastDeathTime > %client.respawnTime)
			%client.spawnPlayer();
	}
};
activatePackage(Support_RespawnTime);