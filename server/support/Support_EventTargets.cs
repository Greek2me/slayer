//----------------------------------------------------------------------
// Title:   Support_EventTargets
// Author:  Greek2me
// Version: 1
// Updated: December 30, 2014
//----------------------------------------------------------------------
// Add new targets to existing input events.
//----------------------------------------------------------------------
// Include this code in your own scripts as an *individual file*
// called "Support_EventTargets.cs". Do not modify this code.
//----------------------------------------------------------------------

if($Support_EventTargets::Version > 1 && !$Debug)
	return;
$Support_EventTargets::Version = 1;

function registerEventTarget(%target, %prerequisite, %targetFindCode)
{
	if(getWordCount(%target) != 2)
	{
		error("ERROR (registerEventTarget): Invalid parameters. Usage: registerEventTarget(\"TargetName TargetClass\",\"PrerequisiteClass\",\"%client.targetFindCode()\")");
		return;
	}

	%isPrerequisite = %prerequisite !$= "" && %prerequisite !$= 0;
	%targetName = getWord(%target, 0);
	%targetClass = getWord(%target, 1);

	%iCount = getFieldCount($InputEvent_ClassList);
	for(%i = 0; %i < %iCount; %i ++) //LOOP THROUGH EVERY CLASS THAT SUPPORTS EVENTS (events can be on more than just bricks, you know)
	{
		%class = getField($InputEvent_ClassList, %i);
		for(%e = 0; %e < $InputEvent_Count[%class]; %e ++) //LOOP THROUGH INPUT EVENTS FOR THAT CLASS
		{
			%canRegister = 1;
			%hasPrereq = 0;
			%fCount = getFieldCount($InputEvent_TargetList[%class, %e]);
			for(%f = 0; %f < %fCount; %f ++) //LOOP THROUGH THE TARGETS FOR THAT INPUT EVENT
			{
				%field = getField($InputEvent_TargetList[%class, %e], %f);
				if(%targetName $= getWord(%field, 0))
				{
					%canRegister = 0;
					break;
				}
				if(%isPrerequisite)
				{
					if(getWord(%field, 1) $= %prerequisite)
					{
						%canRegister = 1;
						%hasPrereq = 1;
					}
					else if(!%hasPrereq)
						%canRegister = 0;
				}
			}

			if(%canRegister)
			{
				$InputEvent_TargetList[%class, %e] = $InputEvent_TargetList[%class, %e] TAB %target;
			}
		}
	}
	if(%targetFindCode !$= "")
	{
		%stripChars = " `~!@#$%^&*()-=+[{]}\\|;:\'\",<.>/?";
		%safeName = stripChars(%targetName, %stripChars);
		$InputEvent_TargetFindCode[%targetClass, %safeName] = %targetFindCode;
	}
}

package Support_EventTargets
{
	function SimObject::processInputEvent(%this, %eventName, %client)
	{
		%class = %this.getClassName();
		%idx = inputEvent_GetInputEventIdx(%eventName);

		%iCount = getFieldCount($InputEvent_TargetList[%class, %idx]);
		%iCount = (%iCount $= "" ? 0 : %iCount);
		for(%i = 0; %i < %iCount; %i ++)
		{
			%field = getField($InputEvent_TargetList[%class, %idx], %i);
			%targetName = getWord(%field, 0);
			%targetClass = getWord(%field, 1);

			%stripChars = " `~!@#$%^&*()-=+[{]}\\|;:\'\",<.>/?";
			%safeName = stripChars(%targetName, %stripChars);

			%targetFindCode = $InputEvent_TargetFindCode[%targetClass, %safeName];
			if(!isObject(%client) && strPos(%targetFindCode, "%client") >= 0)
				continue;
			if(%targetFindCode !$= "")
			{
				$InputTarget_[%targetName] = eval("return" SPC %targetFindCode @ ";");
			}
		}

		return parent::processInputEvent(%this, %eventName, %client);
	}
};
activatePackage(Support_EventTargets);