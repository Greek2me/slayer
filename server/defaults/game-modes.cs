// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//This is the default game mode for new minigames.
new ScriptGroup(Slayer_DefaultGameModeTemplateSG)
{
	class = "Slayer_GameModeTemplateSG";
	className = "Slayer_Deathmatch";
	uiName = "Deathmatch";
	useTeams = false;
};

new ScriptGroup(Slayer_GameModeTemplateSG)
{
	className = "Slayer_TeamDeathmatch";
	uiName = "Team Deathmatch";
	useTeams = true;
};

if($Debug)
{
	//example
	new ScriptGroup(Slayer_GameModeTemplateSG)
	{
		// Game mode settings
		className = "ExampleGameMode";
		uiName = "Example Mode";
		useTeams = true;
		// disable_teamCreation = true;

		// Team settings
		teams_minTeams = 2;
		teams_maxTeams = 4;

		// Default minigame settings
		locked_title = "Slayer";
		default_weaponDamage = false;
		
		// Locked minigame settings
		locked_playerDatablock = nameToID("PlayerStandardArmor");
		locked_lives = 10;
		locked_weaponDamage = true;
		locked_teams_allySameColors = false;
		locked_chat_enableTeamChat = false;
		
		// Teams
		new ScriptObject()
		{
			// Prevent team from being deleted
			disable_delete = true;
			disable_edit = false;
			
			// Default team settings
			default_botFillLimit = 5;
			
			// Locked team settings
			locked_name = "Red";
			locked_lives = 5;
			locked_color = 0;
			locked_sort = true;
		};
		
		new ScriptObject()
		{
			// Prevent team from being deleted
			disable_delete = false;
			disable_edit = false;
			
			// Default team settings
			default_name = "Blue";
			
			// Locked team settings
			locked_color = 3;
			locked_sort = false;
			locked_syncLoadout = true;
		};
		
		new ScriptObject()
		{
			disable_edit = true;
			disable_delete = true;
			default_name = "Green";
			default_color = 2;
		};
	};
}