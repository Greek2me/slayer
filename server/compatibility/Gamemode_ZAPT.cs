// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

if($AddOn__GameMode_ZAPT != 1)
	return;

//Register ZAPT preferences.
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Enable ZAPT!";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.ZAPT_Enabled";
	type = "bool";
	guiTag = "advanced";
	requiresMiniGameReset = 1;
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Gamemode";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Gamemode";
	type = "list";
	list_items = "0 Infection" NL "1 Versus" NL "2 Man vs Tank";
	guiTag = "advanced";
	requiresMiniGameReset = 1;
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Disable Friendly-Fire";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.ZAPT_FF";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Outbreak";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Outbreak";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Spawn Mobs";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Mobs";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Enable Tank Selection";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Tank";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Enable Player Zombies";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_PlayerZombies";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Rescue on Countdown";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Rescue";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Round Timer Enabled";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Timer";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT";
	title = "Allow Suicide";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Suicide";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Points";
	title = "Eating a Player";
	defaultValue = 3;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Points_EatingPlayer";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "advanced";
	notifyPlayersOnChange = false;
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Points";
	title = "Getting Eaten";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Points_GettingEaten";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "advanced";
	notifyPlayersOnChange = false;
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Points";
	title = "Killing a Bot Zombie";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Points_KillBot";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "advanced";
	notifyPlayersOnChange = false;
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Points";
	title = "Killing a Player Zombie";
	defaultValue = 2;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Points_KillPlayer";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "advanced";
	notifyPlayersOnChange = false;
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Points";
	title = "Killing a Tank";
	defaultValue = 10;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Points_KillTank";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "advanced";
	notifyPlayersOnChange = false;
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Points";
	title = "Killing a Teammate";
	defaultValue = -5;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Points_KillTeam";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "advanced";
	notifyPlayersOnChange = false;
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Time";
	title = "Outbreak Timer";
	defaultValue = 30;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Time_Outbreak";
	type = "int";
	int_minValue = 5;
	int_maxValue = 999;
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Time";
	title = "Round Timer";
	defaultValue = 180;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Time_Rescue";
	type = "int";
	int_minValue = 5;
	int_maxValue = 999;
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "ZAPT - Time";
	title = "Reset Timer";
	defaultValue = 10;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.ZAPT_Time_Reset";
	type = "int";
	int_minValue = 5;
	int_maxValue = 30;
	guiTag = "advanced";
	notifyPlayersOnChange = false;
};

function getFakeZAPTMinigameOwner(%mini)
{
	if(isObject(FakeZAPTMinigameOwner))
	{
		return FakeZAPTMinigameOwner;
	}
	else
	{
		return new ScriptObject(FakeZAPTMinigameOwner)
		{
			name = "Slayer";
			netName = "Slayer";
			LANName = "Slayer";
			bl_id = %mini.creatorBLID;
			minigame = %mini;
			brickgroup = nameToID("BrickGroup_" @ %mini.creatorBLID);
		};
	}
}

package Slayer_Compatibility_Gamemode_ZAPT
{
	function Slayer_MinigameSO::onAdd(%this)
	{
		parent::onAdd(%this);

		if($AddOn__GameMode_ZAPT == 1 && isFunction(Slayer_MinigameSO, assembleZombieGame))
		{
			%this.assembleZombieGame();
		}
	}

	function zombieValidTarget(%obj, %target)
	{
		%mini = getMinigameFromObject(%obj);

		if(isSlayerMinigame(%mini) && !isObject(%mini.owner))
		{
			%fake = getFakeZAPTMinigameOwner(%mini);
			%mini.owner = %fake;

			%par = parent::zombieValidTarget(%obj, %target);

			%mini.owner = "";

			return %par;
		}

		return parent::zombieValidTarget(%obj, %target);
	}

	function fxDtsBrick::zombieCanSpawn(%this)
	{
		%mini = getMinigameFromObject(%this);

		if(isSlayerMinigame(%mini) && !isObject(%mini.owner))
		{
			%fake = getFakeZAPTMinigameOwner(%mini);
			%mini.owner = %fake;

			%par = parent::zombieCanSpawn(%this);

			%mini.owner = "";

			return %par;
		}

		return parent::zombieCanSpawn(%this);
	}
};
activatePackage(Slayer_Compatibility_Gamemode_ZAPT);