//SLAYER

//this new gamemode system is not very flexible...

//load default add-ons
ADDON Weapon_Sword
ADDON Weapon_Spear
ADDON Weapon_Rocket_Launcher
ADDON Weapon_Push_Broom
ADDON Weapon_Horse_Ray
ADDON Weapon_Guns_Akimbo
ADDON Weapon_Gun
ADDON Weapon_Bow
ADDON Vehicle_Tank
ADDON Vehicle_Rowboat
ADDON Vehicle_Pirate_Cannon
ADDON Vehicle_Magic_Carpet
ADDON Vehicle_Jeep
ADDON Vehicle_Horse
ADDON Vehicle_Flying_Wheeled_Jeep
ADDON Vehicle_Ball
ADDON Support_Player_Persistence
ADDON Support_Doors
ADDON Sound_Synth4
ADDON Sound_Phone
ADDON Sound_Beeps
ADDON Projectile_Radio_Wave
ADDON Projectile_Pong
ADDON Projectile_Pinball
ADDON Projectile_GravityRocket
ADDON Print_Letters_Default
ADDON Print_2x2r_Default
ADDON Print_2x2f_Default
ADDON Print_1x2f_Default
ADDON Print_1x2f_BLPRemote
ADDON Player_Quake
ADDON Player_No_Jet
ADDON Player_Leap_Jet
ADDON Player_Jump_Jet
ADDON Player_Fuel_Jet
ADDON Particle_Tools
ADDON Particle_Player
ADDON Particle_Grass
ADDON Particle_FX_Cans
ADDON Particle_Basic
ADDON Light_Basic
ADDON Light_Animated
ADDON Item_Sports
ADDON Item_Skis
ADDON Item_Key
ADDON Event_Camera_Control
ADDON Emote_Love
ADDON Emote_Hate
ADDON Emote_Confusion
ADDON Emote_Alarm
ADDON Brick_V15
ADDON Brick_Treasure_Chest
ADDON Brick_Teledoor
ADDON Brick_Large_Cubes
ADDON Brick_Halloween
ADDON Brick_Doors
ADDON Brick_Christmas_Tree
ADDON Brick_Checkpoint
ADDON Brick_Arch
//bots don't load dependencies properly... :/
ADDON Bot_Hole
ADDON Bot_Zombie
ADDON Bot_Shark
ADDON Bot_Horse
ADDON Bot_Blockhead

//load ourselves
ADDON Gamemode_Slayer

//environment
$EnvGuiServer::SimpleMode 0
$EnvGuiServer::SkyFile Add-Ons/Sky_Sunset/Sunset.dml
$EnvGuiServer::WaterFile NONE
$EnvGuiServer::GroundFile Add-Ons/Ground_Plate/plate.ground
$EnvGuiServer::SunFlareTopTexture base/lighting/corona.png
$EnvGuiServer::SunFlareBottomTexture base/lighting/corona.png
$EnvGuiServer::DayOffset 0
$EnvGuiServer::DayLength 300
$EnvGuiServer::DayCycleEnabled 0
$EnvGuiServer::DayCycle Add-Ons/DayCycle_Default/default.daycycle
$EnvGuiServer::SunAzimuth 310.041
$EnvGuiServer::SunElevation 23.4694
$EnvGuiServer::DirectLightColor 0.359813 0.140187 0.112150 1.000000
$EnvGuiServer::AmbientLightColor 0.242991 0.107477 0.102804 1.000000
$EnvGuiServer::ShadowColor 0.158879 0.102804 0.070093 1.000000
$EnvGuiServer::SunFlareColor 0.200000 0.200000 0.200000 1.000000
$EnvGuiServer::SunFlareSize 3.40816
$EnvGuiServer::VisibleDistance 1000
$EnvGuiServer::FogDistance 44.898
$EnvGuiServer::FogHeight 
$EnvGuiServer::FogColor 0.900000 0.719626 0.728972 1.000000
$EnvGuiServer::WaterColor 1.000000 1.000000 1.000000 0.501961
$EnvGuiServer::WaterHeight 5
$EnvGuiServer::UnderWaterColor 0.000000 0.000000 0.501961 0.501961
$EnvGuiServer::SkyColor 1.000000 1.000000 1.000000 1.000000
$EnvGuiServer::WaterScrollX 0
$EnvGuiServer::WaterScrollY 0
$EnvGuiServer::GroundColor 0.000000 0.501961 0.250980 1.000000
$EnvGuiServer::GroundScrollX 0
$EnvGuiServer::GroundScrollY 0
$EnvGuiServer::VignetteMultiply 0
$EnvGuiServer::VignetteColor 0.411215 0.266355 0.252336 0.378505

$GameMode::BrickOwnership Host
