// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_Teams::onWake(%this, %tabSet)
{
	if(%tabSet)
	{
		Slayer_Teams_MovePlayerName.clear();
		Slayer_Teams_MovePlayerName.setText("Select a Player");
		commandToServer('Slayer_SendPlayerList');
	}
	else
	{
		%this.clickCancel();
	}
}

function Slayer_Teams::refresh(%this)
{
	Slayer_Teams_Selector.clear();
	%count = SlayerClient.teams.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%team = SlayerClient.teams.getObject(%i);
		Slayer_Teams_Selector.addRow(%team, %team.name);
	}
}

function Slayer_Teams::clickCreateTeam(%this)
{
	if(SlayerClient.GameModes.currentGameMode.disable_teamCreation)
	{
		showDialogBox("OK",
			"exclamation",
			"Team Creation Disabled",
			"You cannot create new teams in this game mode."
		);
	}
	else if(strLen(%maxTeams = SlayerClient.GameModes.currentGameMode.teams_maxTeams) &&
		SlayerClient.teams.getCount() >= %maxTeams)
	{
		showDialogBox
		(
			"OK",
			"information",
			"Oops!",
			"This game mode only allows you to create" SPC %maxTeams SPC
				(%maxTeams == 1 ? "team" : "teams") @ "."
		);
	}
	else
	{
		%team = SlayerClient.teams.createTeam();
		Slayer_Teams_Selector.setSelectedByID(%team);
		%this.resetDatablockLists();
		Slayer_Teams_Edit.setVisible(true);
	}
}

function Slayer_Teams::clickDeleteTeam(%this)
{
	%team = Slayer_Teams_Selector.getSelectedID();
	%row = Slayer_Teams_Selector.getRowNumByID(%team);
	if(%team._template.disable_delete)
	{
		showDialogBox("OK", "exclamation", "Deletion Disabled",
			"This team cannot be deleted.");
		return;
	}
	if(isObject(%team))
		SlayerClient.Teams.removeTeam(%team);
	%this.clickCancel();
	if(Slayer_Teams_Selector.rowCount() >= %row)
	{
		Slayer_Teams_Selector.setSelectedRow(%row);
	}
}

function Slayer_Teams_Selector::onSelect(%this, %team)
{
	if(%team == Slayer_Teams.currentTeam)
		return;
	if(%team._template.disable_edit)
	{
		//Allow them to view the settings but let them know that it cannot be edited
		showDialogBox("OK", "exclamation", "Editing Disabled",
			"This team cannot be edited. Your changes will have no effect.");
	}

	Slayer_Teams.clickApply();
	if(!isObject(%team))
	{
		showDialogBox("OK", "exclamation", "Error", "Invalid team.");
		return;
	}

	Slayer_Teams.currentTeam = %team;
	for(%i = SlayerClient.TeamPrefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = SlayerClient.TeamPrefs.getObject(%i);
		$Slayer::Client::TeamPref[%pref.clientVariable] = %pref.getValue(%team);
		%var = "$Slayer::Client::TeamPref" @ %pref.clientVariable;
		$Slayer::Client::PrefObj[%var] = %pref;
	}

	Slayer_Teams.loadingTeam = true;
	SlayerClient.teamPrefs.updateGUIValue(Slayer_Teams_Edit, true);
	Slayer_Teams.loadingTeam = false;

	Slayer_Teams_Name.setText("<color:ffffff>Team:" SPC %team.name);
	Slayer_Teams_Color.setColor(getColorIDTable(%team.color));
	Slayer_Teams_Edit.setVisible(true);
	
	%canApply = !%team._template.disable_edit;
	Slayer_Teams_Edit_ApplyBtn.setActive(%canApply);
	Slayer_Teams_Edit_ApplyBtn.enabled = %canApply;
}

function Slayer_Teams_Selector::onDeleteKey(%this, %name)
{
	Slayer_Teams.clickDeleteTeam();
}

function Slayer_Teams_MovePlayerName::refreshList(%this)
{
	%this.clear();
	%this.setText("Select a Player");
	commandToServer('Slayer_SendPlayerList');
}

function Slayer_Teams::clickApply(%this)
{
	if(!Slayer_Teams_Edit.isVisible())
		return;

	%team = %this.currentTeam;
	if(isObject(%team) && !%team._template.disable_edit)
	{
		for(%i = SlayerClient.teamPrefs.getCount() - 1; %i >= 0; %i --)
		{
			%pref = SlayerClient.teamPrefs.getObject(%i);
			%pref.setValue($Slayer::Client::TeamPref[%pref.clientVariable], %team);
		}
		Slayer_Teams_Selector.setRowByID(%team, %team.name);
	}
	
	%this.clickCancel();
}

function Slayer_Teams::clickCancel(%this)
{
	Slayer_Teams_Edit.setVisible(false);
	if(%this.currentTeam == Slayer_Teams_Selector.getSelectedID())
		Slayer_Teams_Selector.clearSelection();
	%this.currentTeam = 0;
	deleteVariables("$Slayer::Client::TeamPref*");
}

function Slayer_Teams::clickAdvanced(%this)
{
	Slayer_Teams_Advanced.editTeam = %this.currentTeam;
	Slayer_Main_Tabs.setTab(Slayer_Teams_Advanced);
}

function Slayer_Teams::resetDatablockLists(%this)
{
	%this.loadingTeam = true;
	for(%i = 0; %i < 5; %i ++)
	{
		%ctrlA = "Slayer_Teams_Edit_StartEquip" @ %i;
		%ctrlB = "Slayer_General_Player_StartEquip" @ %i;
		%ctrlA.setSelected(%ctrlB.getSelected());
	}
	%ctrlA = Slayer_Teams_Edit_Datablock;
	%ctrlB = Slayer_General_Player_Datablock;
	%ctrlA.setSelected(%ctrlB.getSelected());
	%this.loadingTeam = false;
}

function Slayer_Teams::setLoadoutSync(%this, %flag)
{
	if(%this.loadingTeam)
		return;
	$Slayer::Client::TeamPref::Team::Sync_w_Minigame_Loadout = %flag;

	//set the checkbox to the correct value
	if(Slayer_Teams_Edit_sync.getValue() != %flag)
		Slayer_Teams_Edit_sync.setValue(%flag);

	if(%flag)
	{
		%this.resetDatablockLists();
	}
	else
	{
		//if sync is a locked preference, reset everything
		%team = Slayer_Teams_Selector.getSelectedID();
		if(%team._template.locked_syncLoadout)
		{
			%this.setLoadoutSync(true);
		}
	}
}

function Slayer_Teams::createColorMenu(%this, %obj)
{
	if(isObject(Slayer_Teams_ColorMenu))
	{
		Slayer_Teams_ColorMenu.delete();
		return;
	}
	if(isObject(Avatar_ColorMenu))
		Avatar_ColorMenu.delete();
	
	%team = Slayer_Teams_Selector.getSelectedID();
	if(strLen(%team._template.locked_color))
		return;

	WrenchEventsDlg::createColorMenu(%this.getObject(0), %obj);

	Avatar_ColorMenu.setName("Slayer_Teams_ColorMenu");

	Slayer_Teams_Edit.add(Slayer_Teams_ColorMenu);

	%ext = Slayer_Teams_ColorMenu.getExtent();
	%w = getWord(%ext, 0);
	%h = getWord(%ext, 1);

	%objPos = Slayer_Teams_Color.getPosition();
	%objX = getWord(%objPos, 0);
	%objY = getWord(%objPos, 1);

	%objExt = Slayer_Teams_Color.getExtent();
	%objW = getWord(%objExt, 0);
	%objH = getWord(%objExt, 1);

	Slayer_Teams_ColorMenu.resize(%objX+%objW, %objY, %w, %h);

	for(%i=0;%i<64;%i++)
	{
		%num = (%i*2)+1;

		if(%num >= Slayer_Teams_ColorMenu.getObject(0).getCount())
			break;

		%o = Slayer_Teams_ColorMenu.getObject(0).getObject(%num);
		%o.command = "Slayer_Teams.pickColor(" @ %i @ ");";
	}
}

function Slayer_Teams::pickColor(%this, %c)
{
	%team = Slayer_Teams_Selector.getSelectedID();
	if(isObject(Slayer_Teams_ColorMenu))
		Slayer_Teams_ColorMenu.delete();

	Slayer_Teams_Color.value = %c;
	Slayer_Teams_Color.setColor(getColorIDTable(%c));

	$Slayer::Client::TeamPref::Team::Color = Slayer_Teams_Color.value;
}

function Slayer_Teams::clickAddMember(%this)
{
	%team = Slayer_Teams_Selector.getSelectedID();
	%member = Slayer_Teams_movePlayerName.getSelected();
	if(isObject(%team))
	{
		if(%team._serverID > 0)
		{
			if(%member > 0)
			{
				commandToServer('Teams', "addMember", %member, %team.name);
			}
			else
			{
				showDialogBox
				(
					"OK",
					"exclamation",
					"Hang on a second...", 
					"You don't have a player selected.\n\nPlease select a " @
					"player."
				);
			}
		}
		else
		{
			showDialogBox
			(
				"OK",
				"exclamation",
				"Hang on a second...",
				"You can only add members to teams that have already " @
				"been created.\n\nTo create this team, please update " @
				"the minigame and then try again."
			);
		}
	}
	else
	{
		showDialogBox
		(
			"OK",
			"exclamation",
			"Hang on a second...",
			"You don't have a team selected.\n\nPlease select a team."
		);
	}
	Slayer_Teams_MovePlayerName.refreshList();
}

function Slayer_Teams::clickRemoveMember(%this)
{
	%team = Slayer_Teams_Selector.getSelectedID();
	%member = Slayer_Teams_movePlayerName.getSelected();
	if(isObject(%team))
	{
		if(%team._serverID > 0)
		{
			if(%member > 0)
			{
				commandToServer('Teams', "removeMember", %member, %team.name);
			}
			else
			{
				showDialogBox
				(
					"OK",
					"exclamation",
					"Hang on a second...", 
					"You don't have a player selected.\n\nPlease select a " @
					"player."
				);
			}
		}
		else
		{
			showDialogBox
			(
				"OK",
				"exclamation",
				"Hang on a second...",
				"You can only remove members from teams that have already " @
				"been created.\n\nTo create this team, please update " @
				"the minigame and then try again."
			);
		}
	}
	else
	{
		showDialogBox
		(
			"OK",
			"exclamation",
			"Hang on a second...",
			"You don't have a team selected.\n\nPlease select a team."
		);
	}
	Slayer_Teams_MovePlayerName.refreshList();
}

function Slayer_Teams::clickEditUniform(%this)
{
	%team = Slayer_Teams_Selector.getSelectedID();
	if(!isObject(%team))
		return;

	//save the current avatar
	export("$Pref::Avatar::*", $Slayer::Client::ConfigDir @ "/Temp/avatarBackup.cs");
	//load the team's uniform
	%this.loadUniform();

	AvatarGUI.slyrUniTeam = %team;
	canvas.pushDialog(AvatarGUI);
}

function Slayer_Teams::loadUniform(%this)
{
	%teamColor = getColorIDTable($Slayer::Client::TeamPref::Team::Color);

	for(%i = SlayerClient.TeamPrefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = SlayerClient.TeamPrefs.getObject(%i);
		if(%pref.category !$= "Uniform")
			continue;
		%value = $Slayer::Client::TeamPref[%pref.clientVariable];
		if(%value $= "TEAMCOLOR")
			%value = %teamColor;
		$Pref::Avatar["::" @ %pref.title] = %value;
	}
}

function Slayer_Teams::saveUniform(%this)
{
	for(%i = SlayerClient.TeamPrefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = SlayerClient.TeamPrefs.getObject(%i);
		if(%pref.category !$= "Uniform")
			continue;
		%value = $Pref::Avatar["::" @ %pref.title];
		$Slayer::Client::TeamPref[%pref.clientVariable] = %value;
	}

	Slayer_Teams_Edit_Uniform.setSelected(3);

	//this is just an easy way to tell the server that stuff was updated
	%team.team_uniUpdateTrigger = getRandom(-999999, 999999);
}

function Slayer_Teams::resetUniform(%this)
{
	%team = AvatarGUI.slyrUniTeam;
	if(!isObject(%team))
		return;

	%teamColor = getColorIDTable($Slayer::Client::TeamPref::Team::Color);

	for(%i = SlayerClient.TeamPrefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = SlayerClient.TeamPrefs.getObject(%i);
		if(%pref.category !$= "Uniform")
			continue;
		%value = %pref.defaultValue;
		if(%value $= "TEAMCOLOR")
			%value = %teamColor;
		$Pref::Avatar["::" @ %pref.title] = %value;
	}

	Avatar_UpdatePreview();
}

function clientCmdSlayerClient_getPlayerListItem(%id, %name, %teamID)
{
	%team = serverIDToClientID(%teamID);
	if(isObject(%team))
		%text = %name SPC "[" @ %team.name @ "]";
	else
		%text = %name;
	Slayer_Teams_MovePlayerName.add(%text, %id);
}

package Slayer_Client_GUI_Teams
{
	function AvatarGUI::onWake(%this)
	{
		%team = %this.slyrUniTeam;

		if(isObject(%team))
		{
			if(isPackage(MinigameGUIClient)) //TDM causes problems, deactivate it...
				deActivatePackage(MinigameGUIClient);

			Avatar_Prefix.getGroup().setVisible(false);
			Avatar_Name.getGroup().setVisible(false);

			%this.getObject(0).setText("Slayer | Edit Uniform |" SPC %team.name);

			%this.avatarReset = new GuiBitmapButtonCtrl()
			{
				profile = "BlockButtonProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "20 439";
				extent = "151 23";
				minExtent = "8 2";
				visible = "1";
				command = "Slayer_Teams.resetUniform();";
				text = "Reset To Team Default";
				groupNum = "-1";
				buttonType = "PushButton";
				bitmap = $Slayer::Client::GUI::ResourcePath @ "/button1";
				lockAspectRatio = "0";
				alignLeft = "0";
				overflowImage = "0";
				mKeepCached = "0";
				mColor = "255 255 255 255";
			};
			%this.getObject(0).add(%this.avatarReset);
		}

		parent::onWake(%this);
	}

	function AvatarGUI::onSleep(%this)
	{
		parent::onSleep(%this);

		if(isObject(%this.slyrUniTeam))
		{
			Avatar_Prefix.getGroup().setVisible(true);
			Avatar_Name.getGroup().setVisible(true);

			%this.getObject(0).setText("Player Appearance");

			if(isObject(%this.AvatarReset))
				%this.avatarReset.delete();

			%this.slyrUniTeam = "";

			//reload the previous avatar
			exec($Slayer::Client::ConfigDir @ "/Temp/avatarBackup.cs");

			if(isPackage(MinigameGUIClient)) //reactivate TDM...
				activatePackage(MinigameGUIClient);
		}
	}

	function Avatar_Done()
	{
		%team = AvatarGUI.slyrUniTeam;
		if(isObject(%team))
			Slayer_Teams.saveUniform();

		return parent::avatar_done();
	}
};
activatePackage(Slayer_Client_GUI_Teams);