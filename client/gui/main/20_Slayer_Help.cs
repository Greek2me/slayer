// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Client::Help::OfflineFile = $Slayer::Client::Path @
	"/resources/help/Slayer.hfl";
$Slayer::Client::Help::ServerURL = "mods.greek2me.us/help/Gamemode_Slayer.txt";

function Slayer_Help::onWake(%this, %tabSet)
{
	if(%tabSet)
	{
		%help = Slayer_Help.helpText;
		if(%help $= "")
			%this.getHelpFile();
		else
			Slayer_Help_Text.setText(%help);
	}
}

function Slayer_Help::getHelpFile(%this)
{
	%url = $Slayer::Client::Help::ServerURL;
	%method = "GET";
	%savePath = "";
	%className = "SlayerClient_helpTCP";
	connectToURL(%url, %method, %savePath, %className);
}

function SlayerClient_helpTCP::onDone(%this, %error)
{
	if(%error)
	{
		Slayer_Help.helpText = "<just:right><i>Online help content is not available." @
			"\nTCPClient error" SPC %error @ "</i></just>\n";
		Slayer_Help.helpText = parseCustomTML(Slayer_Help.helpText,
			Slayer_Help_Text, "default");
		Slayer_Help_Text.setText(Slayer_Help.helpText);
		Slayer_Help.loadOfflineHelpFile();
	}
	else
		Slayer_Help_Text.setText(Slayer_Help.helpText);
}

function SlayerClient_helpTCP::handleText(%this, %text)
{
	%text = parseCustomTML(%text, Slayer_Help_Text, "default");

	%help = Slayer_Help.helpText;
	if(%help $= "" || %help $= Slayer_Help_Text.text)
		Slayer_Help.helpText = %text;
	else
		Slayer_Help.helpText = %help @ %text;

	Slayer_Help_Text.setText(Slayer_Help.helpText);
}

function Slayer_Help::loadOfflineHelpFile(%this)
{
	if(!isFile($Slayer::Client::Help::OfflineFile))
		return;
	%file = new FileObject();
	%file.openForRead($Slayer::Client::Help::OfflineFile);
	while(!%file.isEOF())
	{
		%text = %file.readLine();
		%text = parseCustomTML(%text, Slayer_Help_Text, "default");
		Slayer_Help.helpText = Slayer_Help.helpText @ %text;
	}
	Slayer_Help_Text.setText(Slayer_Help.helpText);
	%file.close();
	%file.delete();
}

package Slayer_Client_GUI_Help
{
	function HelpText::setText(%this, %text)
	{
		%text = parseCustomTML(%text, %this, "default");
		parent::setText(%this, %text);
	}
};
activatePackage("Slayer_Client_GUI_Help");

SlayerClient_Support::Debug(2, "GUI Scripts Loaded", "Slayer_Help");