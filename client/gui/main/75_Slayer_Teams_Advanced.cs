// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_Teams_Advanced::onWake(%this, %tabSet)
{
	Slayer_Teams_Advanced_bool.setVisible(false);
	Slayer_Teams_Advanced_list.setVisible(false);
	Slayer_Teams_Advanced_string.setVisible(false);
	Slayer_Teams_Advanced_int.setVisible(false);
	Slayer_Teams_Advanced_slide.setVisible(false);
	Slayer_Teams_Advanced_Apply.setVisible(false);

	Slayer_Teams_Advanced_Selector.sort(0);
	Slayer_Teams_Advanced_Selector.clearSelection();

	if(%tabSet)
	{
		%team = %this.editTeam;

		if(isObject(%team))
		{
			Slayer_Teams_Advanced_Selector.clear();
			for(%i = SlayerClient.TeamPrefs.getCount() - 1; %i >= 0; %i --)
			{
				%pref = SlayerClient.TeamPrefs.getObject(%i);
				if(%pref.guiTag $= "advanced")
				{
					%value = %pref.getDisplayValue($Slayer::Client::TeamPref[%pref.clientVariable]);
					Slayer_Teams_Advanced_Selector.addRow(%pref,
						%pref.category TAB %pref.title TAB %value);
				}
			}
			Slayer_Teams_Advanced_Name.setText("<color:ffffff>Advanced Team " @
				"Settings:" SPC $Slayer::Client::TeamPref::Team::Name);
		}
		else
		{
			Slayer_Main_Tabs.setTab(Slayer_Teams);
			showDialogBox("OK", "error", "Error", "Team not found!", "", true);
		}
	}
}

function Slayer_Teams_Advanced_Selector::onSelect(%this, %pref)
{
	Slayer_Teams_Advanced_bool.setVisible(false);
	Slayer_Teams_Advanced_list.setVisible(false);
	Slayer_Teams_Advanced_string.setVisible(false);
	Slayer_Teams_Advanced_int.setVisible(false);
	Slayer_Teams_Advanced_slide.setVisible(false);

	%noEdit = SlayerClient.TeamPrefPermissions.noEdit[%pref.variable];
	if(%noEdit)
	{
		showDialogBox
		(
			"OK",
			"exclamation",
			"Access Denied",
			"You don't have permission to edit that setting. Please contact " @
			"the host for assistance."
		);
		Slayer_Teams_Advanced_Apply.setVisible(false);
		Slayer_Teams_Advanced_Selector.clearSelection();
		return;
	}
	
	%template = Slayer_Teams.currentTeam._template;
	if(strLen(%template.locked_[%pref.variable]) > 0)
	{
		showDialogBox
		(
			"OK",
			"error",
			"Game Mode",
			"The current game mode is preventing you from " @
			"editing this setting."
		);
		Slayer_Advanced_Apply.setVisible(false);
		Slayer_Advanced_Selector.clearSelection();
		return;
	}

	Slayer_Teams_Advanced_Apply.setVisible(true);

	if(%pref.type $= "object")
		%control = Slayer_Teams_Advanced_list;
	else
		%control = "Slayer_Teams_Advanced_" @ %pref.type;
	if(!isObject(%control))
		return;
	%control.setVisible(true);

	%value = $Slayer::Client::TeamPref[%pref.clientVariable];
	switch$(%pref.type)
	{
		case "bool":
			%control.setText(%pref.title);
			%control.setValue(%value);

		case "string":
			%control.maxLength = %pref.string_maxLength;
			%control.setValue(%value);

		case "int":
			%control.minValue = %pref.int_minValue;
			%control.maxValue = %pref.int_maxValue;
			%control.setValue(%value);

		case "slide":
			%control.range = %pref.slide_minValue SPC %pref.slide_maxValue;
			%control.ticks = %pref.slide_numTicks;
			%control.snap = %pref.slide_snapToTicks;
			%control.setValue(%value);

		case "list":
			%control.clear();
			%count = getRecordCount(%pref.list_items);
			for(%i = 0; %i < %count; %i ++)
			{
				%f = getRecord(%pref.list_items, %i);
				%control.add(restWords(%f), firstWord(%f));
			}
			%control.setSelected(%value);

		case "object":
			%control.clear();
			if(!$UINameTableCreated)
				createUINameTable();
			%class = %pref.object_class;
			%count = $ExtUINameTableCount[%class];
			for(%i = 0; %i < %count; %i ++)
			{
				%name = $ExtUINameTableName[%class, %i];
				%id = $ExtUINameTableID[%class, %i];
				%control.add(%name, %id);
			}
			%control.sort();
			if(!%pref.object_cannotBeNull)
				%control.addFront("NONE", 0);
			%control.setSelected(%value);
	}
}

function Slayer_Teams_Advanced::apply(%this, %pref)
{
	Slayer_Teams_Advanced_Apply.setVisible(false);
	if(%pref.type $= "object")
		%control = Slayer_Teams_Advanced_list;
	else
		%control = "Slayer_Teams_Advanced_" @ %pref.type;
	if(!isObject(%control))
		return;
	%control.setVisible(false);

	switch$(%pref.type)
	{
		case "list" or "object":
			%value = %control.getSelected();

		default:
			%value = %control.getValue();
	}
	$Slayer::Client::TeamPref[%pref.clientVariable] = %value;
	%entry = %pref.category TAB %pref.title TAB %pref.getDisplayValue(%value);
	Slayer_Teams_Advanced_Selector.setRowByID(%pref, %entry);
	Slayer_Teams_Advanced_Selector.clearSelection();
}