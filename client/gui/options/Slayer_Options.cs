// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_Options::onWake(%this)
{
	%this.refresh();
	Slayer_Options_bool.setVisible(false);
	Slayer_Options_list.setVisible(false);
	Slayer_Options_string.setVisible(false);
	Slayer_Options_int.setVisible(false);
	Slayer_Options_slide.setVisible(false);
	Slayer_Options_Apply.setVisible(false);
}

function Slayer_Options::refresh(%this)
{
	Slayer_Options_Selector.clear();

	for(%i = SlayerClient.prefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = SlayerClient.prefs.getObject(%i);
		if(%pref.guiTag !$= "Options")
			continue;
		%value = %pref.getValue();
		%display = %pref.getDisplayValue(%value);
		Slayer_Options_Selector.addRow(%pref, %pref.category TAB %pref.title TAB %display);
	}
	Slayer_Options_Selector.sort(0);
}

function Slayer_Options_Selector::onSelect(%this, %pref)
{
	Slayer_Options_bool.setVisible(false);
	Slayer_Options_list.setVisible(false);
	Slayer_Options_string.setVisible(false);
	Slayer_Options_int.setVisible(false);
	Slayer_Options_slide.setVisible(false);
	Slayer_Options_Apply.setVisible(true);

	%control = "Slayer_Options_" @ %pref.type;
	if(!isObject(%control))
		return;
	%control.setVisible(true);

	switch$(%pref.type)
	{
		case "bool":
			%control.setText(%pref.title);
			%control.setValue(%pref.getValue());

		case "string":
			%control.maxLength = %pref.string_maxLength;
			%control.setValue(%pref.getValue());

		case "int":
			%control.minValue = getWord(getField(%entry, 4), 1);
			%control.maxValue = getWord(getField(%entry, 4), 2);
			%control.setValue(%pref.getValue());

		case "slide":
			%control.range = %pref.slide_minValue SPC %pref.slide_maxValue;
			%control.ticks = %pref.slide_numTicks;
			%control.snap = %pref.slide_snapToTicks;
			%control.setValue(%pref.getValue());

		case "list":
			%control.clear();
			%count = getRecordCount(%pref.list_items);
			for(%i = 0; %i < %count; %i ++)
			{
				%f = getRecord(%pref.list_items, %i);
				%control.add(restWords(%f), firstWord(%f));
			}
			%control.setSelected(%pref.getValue());
	}
}

function Slayer_Options::apply(%this, %pref)
{
	Slayer_Options_Apply.setVisible(false);
	%control = Slayer_Options_ @ %pref.type;
	if(!isObject(%control))
		return;
	%control.setVisible(false);

	switch$(%pref.type)
	{
		case "list":
			%value = %control.getSelected();

		default:
			%value = %control.getValue();
	}
	%pref.setValue(%value);
	%entry = %pref.category TAB %pref.title TAB %pref.getDisplayValue(%value);
	Slayer_Options_Selector.setRowByID(%pref, %entry);
	Slayer_Options_Selector.clearSelection();
}