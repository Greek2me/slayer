// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_CtrDisplay::onWake(%this)
{
	%this.setCentered();
}

function Slayer_CtrDisplay::addText(%this, %string)
{
	%string = parseCustomTML(%string, Slayer_CtrDisplay_Text, "default");
	%text = Slayer_CtrDisplay_Text.getText();
	Slayer_CtrDisplay_Text.setText(%text @ %string);
}

function clientCmdSlayer_ctrDisplayInit()
{
	Slayer_CtrDisplay_Text.setText("");
}

function clientCmdSlayer_ctrDisplayAdd(%string)
{
	if(getTag(%string) !$= %string) //see if it's a tagged string
		%string = restWords(%string);
	Slayer_CtrDisplay.addText(%string);
}