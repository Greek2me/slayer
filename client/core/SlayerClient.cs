// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function SlayerClient::onAdd(%this)
{
	%this.isStarting = true;
	%this.add(%this.gameModes	= new ScriptGroup(SlayerClient_GameModeHandlerSG));
	%this.add(%this.prefs		= new ScriptGroup(SlayerClient_PrefHandlerSG));
	%this.add(%this.serverPrefs	= new ScriptGroup(SlayerClient_ServerPrefHandlerSG));
	%this.add(%this.teams		= new ScriptGroup(SlayerClient_TeamHandlerSG));
	%this.add(%this.teamPrefs	= new ScriptGroup(SlayerClient_TeamPrefHandlerSG));
	%this.isStarting = false;
}

//Handles shutdown and cleanup.
function SlayerClient::onRemove(%this)
{
	//clear data
	%this.clearAllData();

	SlayerClient_Support::Debug(0, "Deactivating Slayer");

	//close the debug logger
	if(isObject(%this.debugFO))
		%this.debugFO.close();
}

//Called when the client disconnects from a server.
//@see	SlayerClient::clearAllData
function SlayerClient::onDisconnect(%this)
{
	SlayerClient_Support::Debug(0, "Disconnecting from server");

	%this.serverHasSlayer = false;

	//JOIN MINIGAME GUI
	JMG_Slayer.setVisible(false);
	JMG_Slayer_Default.setVisible(false);
	JMG_List.columns = "0 102 165 493";

	//HIDE GUIS
	clientCmdSlayer_ForceGUI("ALL", false);

	//CLEAR ALL DATA
	%this.clearAllData();
}

//Sends data back to the server, including changed prefs and teams.
//@param	bool reset	Whether the minigame is to be reset.
//@see	SlayerClient_TeamHandlerSO::sendTeams
function SlayerClient::sendData(%this, %reset)
{
	%this.ServerPrefs.sendPreferenceValues(%reset);
}

//Cleans up all client data.
//@see	SlayerClient::onRemove
function SlayerClient::clearAllData(%this)
{
	SlayerClient_Support::Debug(1, "Clearing data");

	%this.serverPrefs.clearPrefs();
	%this.serverPrefs.clearRules("ALL");
	%this.Teams.clearTeams();
	%this.Gamemodes.deleteAll();

	Slayer_Main_Announcements.clearMessages("SERVER");

	for(%i = 0; %i < 5; %i ++)
	{
		%ctrl = Slayer_General_Player_StartEquip @ %i;
		%ctrl.clear();

		%ctrl = Slayer_Teams_Edit_StartEquip @ %i;
		%ctrl.clear();
	}
	Slayer_General_Player_datablock.clear();
	Slayer_Teams_Edit_Datablock.clear();
	Slayer_General_Mode_Selector.clear();
}

//Requests information from server and then opens the Slayer GUI.
//@param	Slayer_MiniGameSO mini	The server object ID of the minigame to be edited.
//@see	Slayer_Main::onWake
//@see	SlayerClient_pushMain
function SlayerClient::editMinigame(%this, %mini)
{
	if(!%this.DataReceived)
		commandToServer('Slayer', "edit", %mini);
	canvas.pushDialog(Slayer_Main);
}

//Handles server-client authentication. 
//@param	string version	The version of Slayer on the server.
//@param	string modVersions	A tab-delimited list containing the versions of server/client mods for Slayer.
function clientCmdSlayer_Handshake(%version, %modVersions)
{
	SlayerClient_Support::Debug(0, "Handshake Received", "Server has version" SPC %version);
	%clVersion = $Slayer::Client::Version;
	%compare = semanticVersionCompare($Slayer::Client::Version, %version);
	if(%compare == 1)
	{
		NewChatSO.addLine("\c0WARNING: \c6You have a \c3newer \c6version of \c3Slayer \c6than the server." SPC
			"Ask the host to update it.");
		%compat = semanticVersionCompare($Slayer::Client::CompatibleVersion, %version);
		if(%compat != 1 && strLen($Slayer::Client::CompatibleVersion))
		{
			NewChatSO.addLine("\c0 + \c6You are still able to use the Slayer GUI, but some features may not work correctly.");
			%clVersion = %version;
		}
	}

	commandToServer('Slayer_Handshake', %clVersion, $Slayer::Client::modVersions);

	//Load the client stuff, just in case it has been deleted
	if(!isObject(SlayerClient))
		$Slayer::Client = new ScriptGroup(SlayerClient);
}

//Called when the server begins sending initial data.
//Initial data is only sent once and contains game mode and pref information.
function clientCmdSlayer_getInitialData_Start()
{
	SlayerClient_Support::Debug(0, "Handshake Accepted", "Receiving initial data...");

	SlayerClient.clearAllData();
	SlayerClient.serverHasSlayer = true;

	JMG_Slayer.setVisible(true);
	JMG_Slayer_Default.setVisible(true);
	JMG_List.columns = "0 102 165 490 450";
}

//Called when all initial data has been received from the server.
function clientCmdSlayer_getInitialData_End()
{
	SlayerClient_Support::Debug(1, "Initial Data Received");

	//create item and player lists
	%group = ServerConnection.isLocal() ? DatablockGroup : ServerConnection;
	if(isObject(%group))
	{
		%count = %group.getCount();
		for(%i = 0; %i < %count; %i ++)
		{
			%obj = %group.getObject(%i);
			%cn = %obj.getClassName();
			//once we hit MissionArea, we've gone too far
			if(%cn $= "MissionArea")
				break;
			if(%obj.uiName $= "")
				continue;
			if(%cn $= "ItemData")
			{
				for(%e = 0; %e < 5; %e ++)
				{
					%ctrl = Slayer_General_Player_StartEquip @ %e;
					%ctrl.add(%obj.uiName, %obj);

					%ctrl = Slayer_Teams_Edit_StartEquip @ %e;
					%ctrl.add(%obj.uiName, %obj);
				}
				SlayerClient_Support::Debug(2, "ItemData Added", %obj TAB %obj.uiName);
			}
			else if(%cn $= "PlayerData")
			{
				Slayer_General_Player_Datablock.add(%obj.uiName, %obj);
				Slayer_Teams_Edit_Datablock.add(%obj.uiName, %obj);
				SlayerClient_Support::Debug(2, "PlayerData Added", %obj TAB %obj.uiName);
			}
		}
	}
	else
	{
		SlayerClient_Support::Error("Unable to generate datablock lists", "Group not found!");
	}

	//add the NONE values and sort lists
	for(%i = 0; %i < 5; %i ++)
	{
		%ctrl = Slayer_General_Player_StartEquip @ %i;
		if(isObject(%ctrl))
		{
			%ctrl.sort();
			%ctrl.addFront("NONE", 0);
		}

		%ctrl = Slayer_Teams_Edit_StartEquip @ %i;
		if(isObject(%ctrl))
		{
			%ctrl.sort();
			%ctrl.addFront("NONE", 0);
		}
	}
	Slayer_General_Player_datablock.sort();
	Slayer_Teams_Edit_datablock.sort();
}

//Called when the server begins sending minigame data.
//@param	Slayer_MiniGameSO mini
//@param	int adminLevel	The client's admin level. H=0; SA=1; A=2; N/A=3
//@param	int minigameState	Use with $Slayer::MinigameState[Edit|Create|None]
function clientCmdSlayer_getData_Start(%mini, %adminLevel, %minigameState)
{
	Slayer_Main_Content_Loading_Status.setText("Receiving Data");
	SlayerClient.minigame = %mini;
	SlayerClient.adminLevel = %adminLevel;
	SlayerClient.minigameState = %minigameState;
	if(%minigameState $= $Slayer::MinigameState["None"])
	{
		Slayer_Main_Buttons_Update.setActive(false);
		Slayer_Main_Buttons_Reset.setActive(false);
		Slayer_Main_Buttons_End.setActive(false);
	}
	else
	{
		if(%minigameState $= $Slayer::MinigameState["Edit"])
		{
			Slayer_Main_Buttons_Update.setText("Update");
			Slayer_Main_Buttons_Reset.setActive(true);
			Slayer_Main_Buttons_End.setActive(true);
		}
		else if(%minigameState $= $Slayer::MinigameState["Create"])
		{
			Slayer_Main_Buttons_Update.setText("Create");
			Slayer_Main_Buttons_Reset.setActive(false);
			Slayer_Main_Buttons_End.setActive(false);
		}
		Slayer_Main_Buttons_Update.setActive(true);
	}
}

//Called when all minigame data has been received.
//@param	bool openGUI	Whether to open the Slayer GUI.
function clientCmdSlayer_getData_End(%openGUI)
{
	SlayerClient.DataReceived = true;
	Slayer_Main_Content_Loading_Status.setText("");
	if(%openGUI)
	{
		if(Slayer_Main.isAwake())
		{
			if(Slayer_Main_Content_Loading.isVisible())
				Slayer_Main_TabList.setSelectedById(0);	
			Slayer_Main.refresh();
		}
		else
		{
			canvas.pushDialog(Slayer_Main);
		}
	}
}

//Contains info on whether the client can edit or create a minigame.
//@param	int minigameState	Use with $Slayer::MinigameState[Edit|Create|None]
function clientCmdSlayer_getMinigameState(%minigameState)
{
	SlayerClient.minigameState = %minigameState;
	if(JMG_Slayer.isVisible())
		JMG_Slayer.onWake();
	if(NewPlayerListGui.isAwake())
		NewPlayerListGui.clickList();
}

package SlayerClient
{
	function disconnectedCleanup(%bool)
	{
		if(isObject(SlayerClient))
		{
			SlayerClient.onDisconnect();
		}
		parent::disconnectedCleanup(%bool);
	}

	function onExit()
	{
		if(isObject(SlayerClient))
			SlayerClient.delete();
		parent::onExit();
	}
};
activatePackage(SlayerClient);