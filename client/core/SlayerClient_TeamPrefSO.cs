// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function SlayerClient_TeamPrefSO::setValue(%this, %value, %team)
{
	if(!isObject(%team))
		return false;
	%proof = %this.idiotProof(%value);
	if(getField(%proof, 0))
		%value = getField(%proof, 1);
	else
		return false;
	Slayer_Support::setDynamicVariable(%team @ "." @ %this.variable, %value);
	return true;
}

function SlayerClient_TeamPrefSO::getValue(%this, %team)
{
	if(!isObject(%team))
		return false;
	return Slayer_Support::getDynamicVariable(%team @ "." @ %this.variable);
}

function SlayerClient_TeamPrefSO::idiotProof(%this, %value)
{
	return Slayer_PrefSO::idiotProof(%this, %value, true);
}

function SlayerClient_TeamPrefSO::getDisplayValue(%this, %value)
{
	return Slayer_PrefSO::getDisplayValue(%this, %value);
}