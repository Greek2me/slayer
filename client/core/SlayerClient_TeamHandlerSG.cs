// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function SlayerClient_TeamHandlerSG::onAdd(%this)
{
	%this.teamRemovalSG = new ScriptGroup();
}

function SlayerClient_TeamHandlerSG::onRemove(%this)
{
	if(isObject(%this.teamRemovalSG))
		%this.teamRemovalSG.delete();
}

function SlayerClient_TeamHandlerSG::addTeam(%this, %team)
{
	SlayerClient_Support::Debug(2, "Team data received", %team.name);
	%this.add(%team);
	Slayer_Teams_Selector.addRow(%team, %team.name);
	//Slayer_TeamList.generateTeamSwatch(%team);
	return %team;
}

//@param	bool noAdd	Whether to not add to the team list.
function SlayerClient_TeamHandlerSG::createTeam(%this, %noAdd)
{
	%team = new ScriptObject()
	{
		class = SlayerClient_TeamSO;
		_serverID = 0;
		name = "New Team";
	};
	SlayerClient.teamPrefs.resetPreferences(%team);
	if(!%noAdd)
		%this.addTeam(%team);
	return %team;
}

function SlayerClient_TeamHandlerSG::removeTeam(%this, %team)
{
	if(%team._serverID > 0)
		%this.teamRemovalSG.add(%team);
	else
		%team.delete();
	if(Slayer_Teams_Selector.getSelectedID() == %team)
		Slayer_Teams_Edit.setVisible(false);
	Slayer_Teams_Selector.removeRowByID(%team);
}

function SlayerClient_TeamHandlerSG::clearTeams(%this, %removeFromServer)
{
	if(%removeFromServer)
	{
		for(%i = %this.getCount() - 1; %i >= 0; %i --)
		{
			%t = %this.getObject(%i);
			%this.removeTeam(%t);
		}
	}
	else
	{
		%this.teamRemovalSG.deleteAll();
		%this.deleteAll();
	}
	Slayer_Teams_Selector.clear();
	//Slayer_TeamList.removeAllTeamSwatches();
}

function SlayerClient_TeamHandlerSG::getTeamFromID(%this, %id)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%t = %this.getObject(%i);
		if(%t._serverID $= %id)
		{
			return %t;
		}
	}
	return 0;
}

function SlayerClient_TeamHandlerSG::getTeamFromName(%this, %name)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%t = %this.getObject(%i);
		if(%t.name $= %name)
		{
			return %t;
		}
	}
	return 0;
}

function SlayerClient_TeamHandlerSG::sendTeams(%this)
{
	for(%i = %this.teamRemovalSG.getCount() - 1; %i >= 0; %i --)
		commandToServer('Slayer_removeTeam', %this.teamRemovalSG.getObject(%i)._serverID);
	%this.teamRemovalSG.deleteAll();

	%teamCount = %this.getCount();
	%prefCount = SlayerClient.TeamPrefs.getCount();
	for(%i = 0; %i < %teamCount; %i ++)
	{
		%team = %this.getObject(%i);

		//get the number of changed prefs
		%count = 0;
		for(%e = 0; %e < %prefCount; %e ++)
		{
			%p = SlayerClient.TeamPrefs.getObject(%e);
			%orig = %team.ORIG_[%p.variable];
			%value = %p.getValue(%team);
			if(%orig !$= %value)
				%count ++;
		}

		//START THE TEAM PREF TRANSFER
		commandToServer('Slayer_getTeamPrefs_Start', %team._serverID, %team.template, %count);

		for(%e = 0; %e < %prefCount; %e ++)
		{
			%p = SlayerClient.TeamPrefs.getObject(%e);
			%orig = %team.ORIG_[%p.variable];
			%value = %p.getValue(%team);
			if(%value !$= %orig)
				commandToServer('Slayer_getTeamPrefs_Tick', %team._serverID, %p._serverID, %value);
		}

		//END THE TEAM PREF TRANSFER
		commandToServer('Slayer_getTeamPrefs_End', %team._serverID);
	}
}

function onClientReceivedObject_SlayerClient_TeamSO(%team)
{
	//Duplicate values.
	%index = 0;
	while((%field = %team.getTaggedField(%index)) !$= "")
	{
		if(strPos(%field, "ORIG_") != 0 && strPos(%field, "_") != 0)
		{
			%fieldName = getField(%field, 0);
			%fieldValue = getFields(%field, 1);
			%team.ORIG_[%fieldName] = %fieldValue;
		}
		%index ++;
	}
	
	%template = nameToID("ServerObject" @ %team.template);
	if(isObject(%template))
		%team._template = %template;

	SlayerClient.teams.addTeam(%team);
	Slayer_Main_Content_Loading_Progress.setValue(
		SlayerClient.Teams.getCount() / SlayerClient.Teams.expectedCount);
}

function clientCmdSlayer_getTeams_Start(%expectedCount)
{
	SlayerClient_Support::Debug(2, "Receiving Teams...");
	SlayerClient.Teams.clearTeams();
	SlayerClient.Teams.expectedCount = %expectedCount;
	Slayer_Main_Content_Loading_Status.setText("Receiving Teams");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

function clientCmdSlayer_getTeams_End()
{
	SlayerClient_Support::Debug(2, "Teams Received");
	SlayerClient.Teams.expectedCount = "";
	Slayer_Main_Content_Loading_Status.setText("");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}
