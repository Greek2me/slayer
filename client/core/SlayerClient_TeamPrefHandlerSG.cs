// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function SlayerClient_TeamPrefHandlerSG::addPref(%this, %pref)
{
	%this.add(%pref);
	%pref.clientVariable = "::" @ getSafeVariableName(%pref.category) @
		"::" @ getSafeVariableName(%pref.title);
	return %pref;
}

function SlayerClient_TeamPrefHandlerSG::getPrefSO(%this, %category, %title)
{
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%p = %this.getObject(%i);
		if(%p.category $= %category && %p.title $= %title)
		{
			return %p;
		}
	}
	
	return 0;
}

function SlayerClient_TeamPrefHandlerSG::resetPreferences(%this, %team)
{
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		%pref.setValue(%pref.defaultValue, %team);
	}
}

function SlayerClient_TeamPrefHandlerSG::clearPrefs(%this)
{
	%this.deleteAll();
	deleteVariables("$Slayer::Client::TeamPref::*");
	Slayer_Teams_Advanced_Selector.clear();
}

//Exports all team preference settings from a specific minigame to a file.
//@param	string path
function SlayerClient_TeamPrefHandlerSG::exportTeamPreferences(%this, %path)
{
	if(!isWriteableFileName(%path))
	{
		Slayer_Support::error("Slayer_TeamPrefHandlerSG::exportTeamPreferences", "Invalid file path" SPC %path);
		return;
	}
	
	%file = new FileObject();
	%file.openForWrite(%path);
	%file.writeLine("SLAYERTEAMCONFIG 1.0");
	
	%teamCount = SlayerClient.teams.getCount();
	for(%i = 0; %i < %teamCount; %i ++)
	{
		%team = SlayerClient.teams.getObject(%i);
		%file.writeLine("");
		for(%j = %this.getCount() - 1; %j >= 0; %j --)
		{
			%pref = %this.getObject(%j);
			%value = %pref.getValue(%team);
			if(%pref.type $= "object")
				%value = isObject(%value) ? %value.uiName : "";
			%file.writeLine(
				"\"" @ expandEscape(%pref.category) @ "\"," @
				"\"" @ expandEscape(%pref.title) @ "\"," @
				"\"" @ expandEscape(%value) @ "\"");
		}
	}
	
	%file.close();
	%file.delete();
}

//Imports team preference settings from a file.
//@param	string path
function SlayerClient_TeamPrefHandlerSG::importTeamPreferences(%this, %path)
{
	if(!isFile(%path))
	{
		Slayer_Support::error("SlayerClient_TeamPrefHandlerSG::importTeamPreferences", "Invalid file path" SPC %path);
		return;
	}
	
	%file = new FileObject();
	%file.openForRead(%path);
	
	if(%file.readLine() $= "SLAYERTEAMCONFIG 1.0")
	{
		%csv = CSVReader(",");
		while(!%file.isEOF())
		{
			%line = %file.readLine();
			if(!strLen(%line))
			{
				%lastLineBlank = true;
				continue;
			}
			if(%lastLineBlank)
			{
				%team = SlayerClient.teams.createTeam();
				%lastLineBlank = false;
			}
			
			%csv.setDataString(%line);
			%category = collapseEscape(%csv.readNextValue());
			%title = collapseEscape(%csv.readNextValue());
			%value = collapseEscape(%csv.readNextValue());
			%pref = %this.getPrefSO(%category, %title);
			if(isObject(%pref) && !%csv.hasNextValue())
			{
				if(%pref.type $= "object")
				{
					if(strLen(%value))
						%value = SlayerClient_Support::getIDFromUiName(%value, %pref.object_class);
					else
						%value = 0;
				}
				%pref.setValue(%value, %team);
			}
			else
				Slayer_Support::error("Invalid config file line", %line);
		}
	}
	else
	{
		Slayer_Support::error("SlayerClient_TeamPrefHandlerSG::importTeamPreferences", "Incompatible preference file" SPC %path);	
	}
	
	%file.close();
	%file.delete();
}

//Update the displayed value to match the actual value.
//@param	GuiControl ctrl
//@param	bool recursive	Whether to affect all children of this control as well.
//@param	GuiControl exclude	A control to exclude.
function SlayerClient_TeamPrefHandlerSG::updateGUIValue(%this, %ctrl, %recursive, %exclude)
{
	if(nameToID(%ctrl) == nameToID(%exclude))
		return;

	if(strPos(%var = %ctrl.variable, "$Slayer::Client::TeamPref::") == 0 ||
		strPos(%var = %ctrl.popupVariable, "$Slayer::Client::TeamPref::") == 0)
	{
		//Update values.
		%value = Slayer_Support::getDynamicVariable(%var);
		%class = %ctrl.getClassName();
		switch$(%class)
		{
			case "GuiPopUpMenuCtrl":
				%ctrl.setSelected(%value);
			case "GuiSliderCtrl":
				%ctrl.setValue(%value);
				Slayer_Support::setDynamicVariable(%var, Slayer_Support::stripTrailingZeros(%value));
			default:
				%ctrl.setValue(%value);
		}
		
		//Disable locked settings.
		%template = Slayer_Teams.currentTeam._template;
		%pref = $Slayer::Client::PrefObj[%var];
		%enabled = (!isObject(%template) || strLen(%template.locked_[%pref.variable]) == 0);
		%ctrl.setActive(%enabled);
		%ctrl.enabled = %enabled;
	}
	
	if(%recursive)
	{
		for(%i = %ctrl.getCount() - 1; %i >= 0; %i --)
		{
			%this.updateGUIValue(%ctrl.getObject(%i), true, %exclude);
		}
	}
}

function onClientReceivedObject_Slayer_TeamPrefPermissionsSO(%container)
{
	if(isObject(SlayerClient.TeamPrefPermissions))
		SlayerClient.TeamPrefPermissions.delete();
	SlayerClient.TeamPrefPermissions = %container;
	SlayerClient.add(SlayerClient.TeamPrefPermissions);
}

function onClientReceivedObject_SlayerClient_TeamPrefSO(%pref)
{
	SlayerClient.TeamPrefs.addPref(%pref);
	Slayer_Main_Content_Loading_Progress.setValue(
		SlayerClient.TeamPrefs.getCount() / SlayerClient.TeamPrefs.expectedCount);
}

function clientCmdSlayer_getTeamPrefs_Start(%expectedCount)
{
	SlayerClient.TeamPrefs.clearPrefs();
	SlayerClient.TeamPrefs.expectedCount = %expectedCount;
	Slayer_Main_Content_Loading_Status.setText("Receiving Team Preferences");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

function clientCmdSlayer_getTeamPrefs_End()
{
	SlayerClient.TeamPrefs.expectedCount = "";
	Slayer_Main_Content_Loading_Status.setText("");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}