//Support_UpdaterMigration
//By Greek2me.

//The purpose of this module is to migrate users to the Support_Updater system.
//This includes downloading and installing Support_Updater.zip.

if(isFile("Add-Ons/Support_Updater.zip") || $supportUpdaterMigration)
	return;
$supportUpdaterMigration = true;

function doSupportUpdaterInstallNotify()
{
	%message = "<linkcolor:ff0000><sPush><font:arial bold:20>Additional Download<sPop>\n\nAn add-on that you recently installed requires you to download <a:mods.greek2me.us/storage/Support_Updater/Support_Updater.zip>Support_Updater</a>."
		SPC "Support_Updater is designed to give you the latest features and fixes from your favorite add-ons."
		NL "\nClick YES to automatically download the new add-on now. If you would like to be prompted later, click NO."
		NL "\n<sPush><font:arial bold:14>Downloading this file gives you continued access to important updates, including patches and security updates."
		NL "\nClicking YES is HIGHLY recommended!<sPop>";
	messageBoxYesNo("", %message, "doSupportUpdaterInstallDownload();");
}

function doSupportUpdaterInstallDownload()
{
	%url = "http://mods.greek2me.us/storage/Support_Updater.zip";
	%method = "GET";
	%downloadPath = "Add-Ons/Support_Updater.zip";
	%className = "supportUpdaterInstallTCP";

	connectToURL(%url, %method, %downloadPath, %className);

	messageBoxOK("Downloading...", "Downloading Support_Updater.");
}

function supportUpdaterInstallTCP::onDone(%this, %error)
{
	if(%error)
	{
		messageBoxOK("", "Error occurred:" SPC %error
			NL "Support_Updater installation will be attempted again at a later time."
			SPC "If the problem persists, contact greek2me@greek2me.us for assistance.");
	}
	else
	{
		messageBoxOK("", "Installation Success\n\nThank you for your patience.\n\n   ~Greek2me");
	}
}

schedule(1000, 0, "doSupportUpdaterInstallNotify");