//----------------------------------------------------------------------
// Title:   Extended UI Name Table
// Author:  Greek2me
// Version: 1
// Updated: February 26, 2016
//----------------------------------------------------------------------
// Extends the default UI name table.
//----------------------------------------------------------------------
// Include this code in your own scripts as an *individual file*
// called "Support_ExtUINameTable.cs". Do not modify this code.
//----------------------------------------------------------------------

if($Support_ExtUINameTable::Version >= 1 && !$Debug)
	return;
$Support_ExtUINameTable::Version = 1;

package ExtUINameTable
{
	function createUiNameTable()
	{
		%created = $UINameTableCreated;
		%parent = parent::createUiNameTable();
		if(!%created)
		{
			deleteVariables("$ExtUINameTable*");
			%dbCount = getDatablockGroupSize();
			for(%i = 0; %i < %dbCount; %i ++)
			{
				%db = getDatablock(%i);
				if(!strLen(%db.uiName))
					continue;
				%class = %db.getClassName();
				if(!strLen($ExtUINameTableCount[%class]))
					$ExtUINameTableCount[%class] = 0;
				$ExtUINameTableName[%class, $ExtUINameTableCount[%class]] = %db.uiName;
				$ExtUINameTableID[%class, $ExtUINameTableCount[%class]] = %db;
				$ExtUINameTableIndex[%class, %db] = $ExtUINameTableCount[%class];
				$ExtUINameTableCount[%class] ++;
			}
		}
		return %parent;
	}
	
	function disconnectedCleanup(%doReconnect)
	{
		%parent = parent::disconnectedCleanup(%doReconnect);
		deleteVariables("$ExtUINameTable*");
		return %parent;
	}
};
activatePackage(ExtUINameTable);