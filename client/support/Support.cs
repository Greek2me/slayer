// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Support::Main = 1;

// +-------------------------+
// | Stuff for finding stuff |
// +-------------------------+

//Searches for an object given a UI name.
//@param	string uiName
//@param	string className	Specifies what class the object must be.
function SlayerClient_Support::getIDFromUiName(%uiName, %className)
{
	%group = (serverConnection.isLocal() ? datablockGroup : serverConnection);
	switch$(%className)
	{
		case "fxDtsBrick": return $uiNameTable[%uiName];
		case "ItemData": return $uiNameTable_Items[%uiName];
		case "ParticleEmitterData": return $uiNameTable_Emitters[%uiName];
		case "WheeledVehicleData" or "FlyingWheeledVehicleData" or "VehicleData": return $uiNameTable_Vehicle[%uiName];
		case "ScriptGroup":
			for(%i = SlayerClient.gameModes.getCount() - 1; %i >= 0; %i --)
			{
				%obj = SlayerClient.gameModes.getObject(%i);
				if(%obj.uiName $= %uiName)
					return %obj._serverID;
			}
		case "":
			%count = %group.getCount();
			for(%i = 0; %i < %count; %i ++)
			{
				%obj = %group.getObject(%i);
				if(!strLen(%obj.uiName))
					continue;
				%cn = %obj.getClassName();
				if(%cn $= "MissionArea")
					break;
				if(%obj.uiName $= %uiName)
					return %obj;
			}
		default:
			%count = %group.getCount();
			for(%i = 0; %i < %count; %i ++)
			{
				%obj = %group.getObject(%i);
				if(!strLen(%obj.uiName))
					continue;
				%cn = %obj.getClassName();
				if(%cn $= "MissionArea")
					break;
				if(%obj.uiName $= %uiName && %cn $= %className)
					return %obj;
			}
	}
	return 0;
}

// +-------+
// | Debug |
// +-------+
function SlayerClient_Support::Debug(%level, %title, %value)
{
	if($Slayer::Client::Debug == -1)
		return;

	if(%value $= "")
		%val = %title;
	else
		%val = %title @ ":" SPC %value; 

	if(%level <= $Slayer::Client::Debug)
		echo("\c4Slayer (Client):" SPC %val);

	%path = $Slayer::Client::ConfigDir @ "/debug_client.log";
	if(isWriteableFileName(%path))
	{
		if(!isObject(SlayerClient.DebugFO))
		{
			SlayerClient.debugFO = new fileObject();
			SlayerClient.add(SlayerClient.debugFO);

			SlayerClient.debugFO.openForWrite(%path);
			SlayerClient.debugFO.writeLine("//Slayer Version" SPC $Slayer::Client::Version SPC "-----" SPC getDateTime());
			SlayerClient.debugFO.writeLine("//By Greek2me, Blockland ID 11902");
		}

		SlayerClient.debugFO.writeLine(%val);
	}
}

function SlayerClient_Support::Error(%title, %value)
{
	if(%value $= "")
		%val = %title;
	else
		%val = %title @ ":" SPC %value; 

	error("Slayer Error (Client):" SPC %val);

	%path = $Slayer::Client::ConfigDir @ "/debug_client.log";
	if(isWriteableFileName(%path))
	{
		if(!isObject(SlayerClient.debugFO))
		{
			SlayerClient.debugFO = new fileObject();
			SlayerClient.add(SlayerClient.debugFO);

			SlayerClient.debugFO.openForWrite(%path);
			SlayerClient.debugFO.writeLine("//Slayer Version" SPC $Slayer::Client::Version SPC "-----" SPC getDateTime());
			SlayerClient.debugFO.writeLine("//By Greek2me, Blockland ID 11902");
		}

		SlayerClient.debugFO.writeLine("ERROR:" SPC %val);
	}
}