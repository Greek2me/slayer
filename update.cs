// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//If the previous version was lower than 4.0.0
if(semanticVersionCompare($versionOld__Gamemode_Slayer, "4.0.0") == 2)
{
	if(isFile("config/server/slayer/config_preload.cs"))
		fileDelete("config/server/slayer/config_preload.cs");
	if(isFile("config/server/slayer/config_last.cs"))
		fileDelete("config/server/slayer/config_last.cs");
}