// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//Determines the average run time of a function.
//@param	int	iterations	Increase for higher accuracy.
//@param	string function
function Slayer_Support::Benchmark(%iterations, %function, %a, %b, %c, %d, %e, %f,%g, %h, %i, %j, %k, %l, %m, %n, %o)
{
	if(!isFunction(%function))
	{
		warn("BENCHMARKING RESULT FOR" SPC %function @ ":" NL "Function does not exist.");
		return -1;
	}
	%start = getRealTime();
	for(%i = 0; %i < %iterations; %i ++)
	{
		call(%function, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o);
	}
	%end = getRealTime();
	%result = (%end - %start) / %iterations;
	warn("BENCHMARKING RESULT FOR" SPC %function @ ":" NL %result);
	return %result;
}

//Loads files in a directory.
//@param	string path	Execute files in this folder.
//@param	string prefix	Only files with this prefix will load.
//@param	bool excludeSubfolders	Do not load files within subfolders.
//@param	string exclude	A tab-delimited list of file names to exclude. (example.gui TAB blah.cs)
function Slayer_Support::loadFiles(%path, %prefix, %excludeSubFolders, %exclude)
{
	if(strPos(%path, "*") <= 0)
		%path = %path @ "*";

	%filePath = filePath(%path);

	for(%file = findFirstFile(%path); %file !$= ""; %file = findNextFile(%path))
	{
		if(%excludeSubFolders && striCmp(%filePath, filePath(%file)) != 0)
			continue;

		%fileExt = fileExt(%file);
		if(%fileExt !$= ".cs" && %fileExt !$= ".gui" && %fileExt !$= ".mis")
			continue;

		%fileName = fileName(%file);
		if(%prefix !$= "" && striPos(%fileName, %prefix) != 0)
			continue;

		%exc = false;
		for(%i = 0; %i < getFieldCount(%exclude); %i ++)
		{
			if(getField(%exclude, %i) $= %fileName)
			{
				%exc = true;
				break;
			}
		}
		if(%exc)
			continue;

		exec(%file);
	}
}

//Convert color in RGB format to hexadecimal.
//@param	string rgb
//@return	string
function Slayer_Support::RgbToHex(%rgb)
{
	//use % to find remainder
	%r = getWord(%rgb, 0);
	if(%r <= 1)
		%r = %r * 255;
	%g = getWord(%rgb, 1);
	if(%g <= 1)
		%g = %g * 255;
	%b = getWord(%rgb, 2);
	if(%b <= 1)
		%b = %b * 255;
	%a = "0123456789ABCDEF";

	%r = getSubStr(%a, (%r-(%r % 16))/16, 1) @ getSubStr(%a, (%r % 16), 1);
	%g = getSubStr(%a, (%g-(%g % 16))/16, 1) @ getSubStr(%a, (%g % 16), 1);
	%b = getSubStr(%a, (%b-(%b % 16))/16, 1) @ getSubStr(%a, (%b % 16), 1);

	return %r @ %g @ %b;
}

//Find the closest paint color to a given color.
//@param	string rgba RGB color.
//@return	string
function Slayer_Support::getClosestPaintColor(%rgba)
{
	%prevdist = 100000;
	%colorMatch = 0;
	for(%i = 0; %i < 64; %i++)
	{
		%color = getColorIDTable(%i);
		if(vectorDist(%rgba, getWords(%color, 0, 2)) < %prevdist && getWord(%rgba, 3) - getWord(%color, 3) < 0.3 && getWord(%rgba, 3) - getWord(%color, 3) > -0.3)
		{
			%prevdist = vectorDist(%rgba, %color);
			%colormatch = %i;
		}
	}
	return %colormatch;
}

//Calculates the average of several RGB colors.
//@return	string
function Slayer_Support::getAverageColor(%c0, %c1, %c2, %c3, %c4, %c5, %c6, %c7, %c8, %c9, %c10, %c11, %c12, %c13, %c14, %c15, %c16, %c18, %c19)
{
	%red = 0;
	%green = 0;
	%blue = 0;
	%alpha = 0;
	%count = 0;

	for(%i = 0; %i < 20; %i ++) //20 is the max number of arguments in a function
	{
		if(%c[%i] $= "")
		{
			break;
		}
		else
		{
			%red += getWord(%c[%i], 0);
			%green += getWord(%c[%i], 1);
			%blue += getWord(%c[%i], 2);
			%alpha += getWord(%c[%i], 3);
			%count ++;
		}
	}

	%red = %red / %count;
	%green = %green / %count;
	%blue = %blue / %count;
	%alpha = %alpha / %count;

	return %red SPC %green SPC %blue SPC %alpha;
}

//Whether the number is a floating-point.
//@return	bool
function Slayer_Support::isFloat(%string)
{
	%search = "-0123456789.";
	%string = stripChars(%string, %search);
	return (%string $= "");
}

//Removes trailing zeroes from a floating-point number.
function Slayer_Support::stripTrailingZeros(%flag)
{
	if(!Slayer_Support::isFloat(%flag))
		return %flag;
	return %flag + 0;
}

//@return	bool
function Slayer_Support::isItemInList(%list, %item)
{
	for(%i=0; %i < getFieldCount(%list); %i++)
	{
		if(getField(%list, %i) $= %item)
			return 1;
	}

	return 0;
}

//Swaps two items in a tab-delimited list.
function Slayer_Support::swapItemsInList(%list, %index1, %index2)
{
	if(%index1 >= 0 && %index2 >= 0)
	{
		%item1 = getField(%list, %index1);
		%item2 = getField(%list, %index2);

		%list = setField(%list, %index1, %item2);
		%list = setField(%list, %index2, %item1);
	}

	return %list;
}

function Slayer_Support::setDynamicVariable(%var, %val)
{
	if(%var !$= stripChars(%var, " `~!@#^&*-=+{}\\|;\'\",<>/?"))
	{
		Slayer_Support::Error("Slayer_Support::setDynamicVariable", "Invalid variable.");
		return;
	}
	eval(%var SPC "= \"" @ expandEscape(%val) @ "\";");
	return %val;
}

function Slayer_Support::getDynamicVariable(%var)
{
	if(%var !$= stripChars(%var, " `~!@#^&*-=+{}\\|;\'\",<>/?"))
	{
		Slayer_Support::Error("Slayer_Support::setDynamicVariable", "Invalid variable.");
		return;
	}
	%val = eval("return" SPC collapseEscape(%var) @ ";");
	return %val;
}