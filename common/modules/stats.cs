function Slayer_postStats()
{
	%method = "POST";
	%server = "mods.greek2me.us";
	%port = 80;
	%path = "/statistics/Gamemode_Slayer.php";
	%query = "name=" @ urlEnc($Pref::Player::NetName) @
		"&version=" @ urlEnc($Slayer::Version) @
		"&type=" @ urlEnc($Slayer::Platform);
	TCPClient(%method, %server, %port, %path, %query);
}
Slayer_postStats();