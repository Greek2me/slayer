@echo off
::#########
:: Slayer windows zip creator
::#########
:: This file is run to create a zip file in Windows
::
:: Requirements:
:: 7zip (Windows does not include any native form to create a zip file)
:: 7zip path need to be placed in Path environment variable

:: GameMode_Slayer
set ZIP_NAME=Gamemode_Slayer.zip
7z a -mx9 -tzip -r .\%ZIP_NAME% . -x!*.bat -x!*.rst -x!*.markdown -x!*.*~ -x!*.mkdn -x!*.md -x!.* -x!*.zip

:: Client_Slayer
set ZIP_NAME=Client_Slayer.zip
7z a -mx9 -tzip -r .\%ZIP_NAME% . -x!doc -x!*.bat -x!*.rst -x!*.markdown -x!*.*~ -x!*.mkdn -x!*.md -x!.* -x!*.zip -xr!server -x!server.cs -x!colorSet.txt -x!gamemode.txt -x!preview.jpg -x!save.bls -x!thumb.jpg -x!slayer.mgame.csv -x!slayer.teams.csv